<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contractor extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->load->model( 'ContractorModel' );

        check_contractor_login();
        if($this->session->userdata('contractor'))
            $this->data['contractor'] = $this->session->userdata('contractor');

    }

    function index()
    {

        $landlord_id = (int) $this->ContractorModel->get_landlord_id( (int) $this->data['contractor']['user_id'] )[0]['landlord_id'];
        // $unresolved_count = $this->ContractorModel->get_count_unresolved_tickets((int)$user['user_id'], $first, $last);
        // $resolved_count = $this->ContractorModel->get_count_resolved_tickets((int)$user['user_id'], $first, $last);
        // $total_tickets_count = $this->ContractorModel->get_total_tickets((int)$user['user_id'], $first, $last);
        // $recently_rasied_tickets = $this->ContractorModel->get_recently_rasied_tickets((int)$user['user_id'], $first, $last);
        // $oldest_unresolved_tickets = $this->ContractorModel->get_oldest_unresolved_tickets((int)$user['user_id'], $first, $last);
        // $data['unresolved_tickets_count'] = $unresolved_count;
        // $data['resolved_tickets_count'] = $resolved_count;
        // $data['total_tickets'] = $total_tickets_count;
        // $data['recently_rasied_tickets'] = $recently_rasied_tickets;
        // $data['oldest_unresolved_tickets'] = $oldest_unresolved_tickets;
        $outstandingTickets         = $this->ContractorModel->get_outstanding_tickets( $landlord_id, '0' );
        $outstandingTicketsCount    = $this->ContractorModel->get_outstanding_tickets( $landlord_id );
        $this->data['outstandingTickets'] = $outstandingTickets;
        $this->data['pageCount']          = ceil( count( $outstandingTicketsCount )/3 );
        $this->contractor_view('contractorProfile');
	}

	function search_outstanding_tickets()
	{
		$user        = $this->session->userdata( 'user' );
		$this->load->model( 'ContractorModel' );
		$landlord_id = (int) $this->ContractorModel->get_landlord_id( (int) $user['user_id'] )[0]['landlord_id'];
		$value       = $this->input->post( 'inputval' );

		$order_by = $this->input->post( 'orderBy' );
		if ( $order_by )
		{
			$order_by = $this->input->post( 'orderBy' );
		}
		else
		{
			$order_by = '';
		}
		$desc           = $this->input->post( 'desc' );
		$filterVal      = $this->input->post( 'filterValue' );
		$filterCategory = $this->input->post( 'filterCategory' );

		if ( !$filterVal )
		{
			$filterVal = '';
		}
		else
		{
			$filterVal = $this->input->post( 'filterValue' );
		}

		if ( !$filterCategory )
		{
			$filterCategory = '';
		}else
		{
			$filterCategory = $this->input->post( 'filterCategory' );
		}

		if ( $filterCategory == 'C' && $filterVal != '' )
		{
			$filterWhere = 'ticket_category_id';
		}
		else if ( $filterCategory == 'P' && $filterVal != '' )
		{
			$filterWhere = 'tickets.priority';
		}
		else
		{
			$filterWhere = '';
		}

		if ( !$desc )
		{
			$desc = '';
		}
		else
		{
			$desc = $this->input->post( 'desc' );
		}

		$outstandingTicketsCount = $this->ContractorModel->search_outstanding_tickets( $landlord_id, $value, $order_by, $desc, $filterVal, $filterWhere, '999' );
		$result['pageCount']     = ceil( count( $outstandingTicketsCount )/3 );
		$result['data']          = $this->ContractorModel->search_outstanding_tickets( $landlord_id, $value, $order_by, $desc, $filterVal, $filterWhere, '0' );
		$result                  = json_encode( $result );
		print $result;
	}

	function sort_outstanding_tickets()
	{
		$user        = $this->session->userdata( 'user' );
		$this->load->model( 'ContractorModel' );
		$landlord_id = (int) $this->ContractorModel->get_landlord_id( (int) $user['user_id'] )[0]['landlord_id'];
		$order_by = $this->input->post( 'orderBy' );

		if ( $order_by )
		{
			$order_by = $this->input->post('orderBy');
		}
		else
		{
			$order_by = '';
		}
		$search         = $this->input->post( 'searchVal' );
		$desc           = $this->input->post( 'desc' );
		$filterVal      = $this->input->post( 'filterValue' );
		$filterCategory = $this->input->post( 'filterCategory' );

		if ( !$filterVal )
		{
			$filterVal = '';
		}
		else
		{
			$filterVal = $this->input->post( 'filterValue' );
		}

		if ( !$filterCategory )
		{
			$filterCategory = '';
		}
		else
		{
			$filterCategory = $this->input->post( 'filterCategory' );
		}

		if ( $filterCategory == 'C' && $filterVal != '' )
		{
			$filterWhere = 'ticket_category_id';
		}
		else if ( $filterCategory == 'P' && $filterVal != '' )
		{
			$filterWhere = 'tickets.priority';
		}
		else
		{
			$filterWhere = '';
		}

		if( !$search )
		{
			$search = '0';
		}
		else
		{
			$search = $this->input->post( 'searchVal' );
		}

		if ( !$desc )
		{
			$desc = '';
		}
		else
		{
			$desc = $this->input->post( 'desc' );
		}

		$outstandingTicketsCount = $this->ContractorModel->sort_outstanding_tickets( $landlord_id, $order_by, $search, $desc, $filterVal, $filterWhere, '999' );
		$result['pageCount']     = ceil( count( $outstandingTicketsCount )/3 );
		$result['data']          = $this->ContractorModel->sort_outstanding_tickets( $landlord_id, $order_by, $search, $desc, $filterVal, $filterWhere, '0' );
		$result                  = json_encode( $result );
		print $result;
	}

	function filter_outstanding_tickets()
	{
		$user        = $this->session->userdata( 'user' );
		$this->load->model( 'ContractorModel' );
		$landlord_id = (int) $this->ContractorModel->get_landlord_id( (int) $user['user_id'] )[0]['landlord_id'];
		$filterValue = $this->input->post( 'filterValue' );
		$by          = $filterValue[1];
		$order_by    = $this->input->post( 'orderBy' );

		if ( $order_by )
		{
			$order_by = $this->input->post( 'orderBy' );
		}
		else
		{
			$order_by = '';
		}
		$search = $this->input->post( 'searchVal' );
		$desc   = $this->input->post( 'desc' );

		if ( !$search )
		{
			$search = '0';
		}
		else
		{
			$search = $this->input->post( 'searchVal' );
		}

		if ( !$desc )
		{
			$desc = '';
		}
		else
		{
			$desc = $this->input->post( 'desc' );
		}

		if ( $by == 'C' )
		{
			$outstandingTicketsCount = $this->ContractorModel->filter_outstanding_tickets_by_category( $landlord_id, $filterValue[0], $order_by, $search, $desc, '999' );
			$result['pageCount']     = ceil( count( $outstandingTicketsCount )/3 );
			$result['data']          = $this->ContractorModel->filter_outstanding_tickets_by_category( $landlord_id, $filterValue[0], $order_by, $search, $desc, '0' );
		}
		else if ( $by == 'P' )
		{
			$outstandingTicketsCount = $this->ContractorModel->filter_outstanding_tickets_by_priority( $landlord_id, $filterValue[0], $order_by, $search, $desc, '999' );
			$result['pageCount']     = ceil( count( $outstandingTicketsCount )/3 );
			$result['data']          = $this->ContractorModel->filter_outstanding_tickets_by_priority( $landlord_id, $filterValue[0], $order_by, $search, $desc, '0' );
		}
		$result = json_encode( $result );
		print $result;
	}

	function pagination_outstanding_tickets()
	{
		$user        = $this->session->userdata( 'user' );
		$this->load->model( 'ContractorModel' );
		$landlord_id = (int) $this->ContractorModel->get_landlord_id( (int) $user['user_id'] )[0]['landlord_id'];
		$order_by = $this->input->post( 'orderBy' );

		if ( $order_by )
		{
			$order_by = $this->input->post( 'orderBy' );
		}
		else
		{
			$order_by = '';
		}

		$value          = $this->input->post( 'inputval' );
		$page           = $this->input->post( 'page' );
		$search         = $this->input->post( 'searchVal' );
		$desc           = $this->input->post( 'desc' );
		$filterVal      = $this->input->post( 'filterValue' );
		$filterCategory = $this->input->post( 'filterCategory' );

		if ( !$filterVal )
		{
			$filterVal = '';
		}
		else
		{
			$filterVal = $this->input->post( 'filterValue' );
		}

		if ( !$filterCategory )
		{
			$filterCategory = '';
		}
		else
		{
			$filterCategory = $this->input->post( 'filterCategory' );
		}

		if ( $filterCategory == 'C' && $filterVal != '' )
		{
			$filterWhere = 'ticket_category_id';
		}
		else if ( $filterCategory == 'P' && $filterVal != '' )
		{
			$filterWhere = 'tickets.priority';
		}
		else
		{
			$filterWhere = '';
		}

		if ( !$search )
		{
			$search = '0';
		}
		else
		{
			$search = $this->input->post( 'searchVal' );
		}

		if ( !$desc )
		{
			$desc = '';
		}
		else
		{
			$desc = $this->input->post( 'desc' );
		}

		$outstandingTicketsCount = $this->ContractorModel->pagination_outstanding_tickets( $landlord_id, $value, $order_by, $desc, $filterVal, $filterWhere, 999 );
		$result['pageCount']     = ceil( count( $outstandingTicketsCount )/3 );
		$result['data']          = $this->ContractorModel->pagination_outstanding_tickets( $landlord_id, $value, $order_by, $desc, $filterVal, $filterWhere, $page );
		$result                  = json_encode( $result );
		print $result;
	}
}