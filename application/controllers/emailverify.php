<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Emailverify extends CI_Controller {
	function verify($code, $id){
		$this->load->model('UserModel');
		$email_code = $this->UserModel->getEmailCode(array(
				'user_id' => $id
			)
		);
		if($code == $email_code[0]['email_code']){
			$this->UserModel->update(array('registered' => 1, 'email_code' => NULL), $id);
			$this->session->set_flashdata('success', 'Your email is veryfied');
			redirect('login');
		}else{
			$this->session->set_flashdata('error', 'The email verification code is wrong');
			redirect('login');
		}
	}
}