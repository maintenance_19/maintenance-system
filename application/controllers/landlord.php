<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Landlord extends MY_Controller {

    function __construct(){
        parent::__construct();
        $this->load->model('LandlordModel');
    }

    function index(){
        $user = $this->session->userdata('user');
        $first = date('Y-m-d', time() - 2764800);
        $last = date('Y-m-d', time() + 86400);
        // $unresolved_count = $this->LandlordModel->get_count_unresolved_tickets((int)$user['user_id'], $first, $last);
        // $resolved_count = $this->LandlordModel->get_count_resolved_tickets((int)$user['user_id'], $first, $last);
        // $total_tickets_count = $this->LandlordModel->get_total_tickets((int)$user['user_id'], $first, $last);
        // $recently_rasied_tickets = $this->LandlordModel->get_recently_rasied_tickets((int)$user['user_id'], $first, $last);
        // $oldest_unresolved_tickets = $this->LandlordModel->get_oldest_unresolved_tickets((int)$user['user_id'], $first, $last);
        // $data['unresolved_tickets_count'] = $unresolved_count;
        // $data['resolved_tickets_count'] = $resolved_count;
        // $data['total_tickets'] = $total_tickets_count;
        // $data['recently_rasied_tickets'] = $recently_rasied_tickets;
        // $data['oldest_unresolved_tickets'] = $oldest_unresolved_tickets;
        $unallocatedTickets = $this->LandlordModel->get_unallocated_tickets((int)$user['user_id'], $first, $last, '0');
        $unallocatedTicketsCount = $this->LandlordModel->get_unallocated_tickets_count((int)$user['user_id'], $first, $last);
        $inProgressTickets = $this->LandlordModel->get_inprogress_tickets((int)$user['user_id'], '0');
        $inProgressTicketsCount = $this->LandlordModel->get_inprogress_tickets((int)$user['user_id']);
        // $inProgressTickets = $this->LandlordModel->get_in_progress_tickets((int)$user['user_id'], $first, $last);
        $data['unallocatedTickets'] = $unallocatedTickets;
        $data['pageCount'] = ceil(count($unallocatedTicketsCount)/3);
        $data['inProgressTickets'] = $inProgressTickets;
        $data['pageCount2'] = ceil(count($inProgressTicketsCount)/3);
        $this->load->view('landlordProfile', array('data' => $data));
    }

    function team(){
        $user = $this->session->userdata('user');
        $team_members = $this->LandlordModel->get_team_members($user['user_id']);
        $data['team_members'] = $team_members;
        $this->load->view('landlord/team', array('data' => $data));
    }

    function search_unallocated_tickets(){

        $user           = $this->session->userdata('user');
        $first          = date('Y-m-d', time() - 2764800);
        $last           = date('Y-m-d', time() + 86400);

        $value          = $this->input->post('inputval');
        $order_by       = check_null($this->input->post('orderBy'));
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));

        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $unallocatedTicketsCount = $this->LandlordModel->search_unallocated_tickets((int)$user['user_id'], $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '999');
        $result['pageCount']     = ceil(count($unallocatedTicketsCount)/3);
        $result['data']          = $this->LandlordModel->search_unallocated_tickets((int)$user['user_id'], $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '0');

        print (json_encode($result));
    }

    function search_inprogress_tickets(){

        $user           = $this->session->userdata('user');
        $value          = $this->input->post('inputval');
        $order_by       = check_null($this->input->post('orderBy'));
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));

        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if ($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $inProgressTicketsCount = $this->LandlordModel->search_inprogress_tickets((int)$user['user_id'], $value, $order_by, $desc, $filterVal, $filterWhere, '999');
        $result['pageCount2']   = ceil(count($inProgressTicketsCount)/3);
        $result['data']         = $this->LandlordModel->search_inprogress_tickets((int)$user['user_id'], $value, $order_by, $desc, $filterVal, $filterWhere, '0');

        print (json_encode($result));

    }

    function sort_unallocated_tickets(){

        $user  = $this->session->userdata('user');
        $first = date('Y-m-d', time() - 2764800);
        $last  = date('Y-m-d', time() + 86400);

        $order_by       = check_null($this->input->post('orderBy'));
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));
        $search         = check_null($this->input->post('searchVal') , '0');


        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if ($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $unallocatedTicketsCount = $this->LandlordModel->sort_unallocated_tickets((int)$user['user_id'], $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '999');
        $result['pageCount']     = ceil(count($unallocatedTicketsCount)/3);
        $result['data']          = $this->LandlordModel->sort_unallocated_tickets((int)$user['user_id'], $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '0');

        print (json_encode($result));

    }

    function sort_inprogress_tickets(){

        $user = $this->session->userdata('user');

        $order_by       = check_null($this->input->post('orderBy'));
        $search         = check_null($this->input->post('searchVal') , '0');
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));

        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if ($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $inProgressTicketsCount = $this->LandlordModel->sort_inprogress_tickets((int)$user['user_id'], $order_by, $search, $desc, $filterVal, $filterWhere, '999');
        $result['pageCount2']   = ceil(count($inProgressTicketsCount)/3);
        $result['data']         = $this->LandlordModel->sort_inprogress_tickets((int)$user['user_id'], $order_by, $search, $desc, $filterVal, $filterWhere, '0');

        print (json_encode($result));

    }

    function filter_unallocated_tickets(){

        $user  = $this->session->userdata('user');
        $first = date('Y-m-d', time() - 2764800);
        $last  = date('Y-m-d', time() + 86400);

        $filterValue    = $this->input->post('filterValue');
        $order_by       = check_null($this->input->post('orderBy'));
        $search         = check_null($this->input->post('searchVal') , '0');
        $filterVal      = check_null($this->input->post('filterValue'));

        $by = $filterValue[1];

        if ($by == 'C') {
            $unallocatedTicketsCount  = $this->LandlordModel->filter_unallocated_tickets_by_category((int)$user['user_id'], $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
            $result['pageCount']      = ceil(count($unallocatedTicketsCount)/3);
            $result['data']           = $this->LandlordModel->filter_unallocated_tickets_by_category((int)$user['user_id'], $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
        } else if ($by == 'P') {
            $unallocatedTicketsCount  = $this->LandlordModel->filter_unallocated_tickets_by_priority((int)$user['user_id'], $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
            $result['pageCount']      = ceil(count($unallocatedTicketsCount)/3);
            $result['data']           = $this->LandlordModel->filter_unallocated_tickets_by_priority((int)$user['user_id'], $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
        }

        print (json_encode($result));

    }

    function filter_inprogress_tickets(){

        $user = $this->session->userdata('user');

        $filterValue    = $this->input->post('filterValue');
        $order_by       = check_null($this->input->post('orderBy'));
        $search         = check_null($this->input->post('searchVal') , '0');
        $filterVal      = check_null($this->input->post('filterValue'));

        $by = $filterValue[1];

        if ($by == 'C') {
            $inProgressTicketsCount = $this->LandlordModel->filter_inprogress_tickets_by_category((int)$user['user_id'], $filterValue[0], $order_by, $search, $desc, '999');
            $result['pageCount2']   = ceil(count($inProgressTicketsCount)/3);
            $result['data']         = $this->LandlordModel->filter_inprogress_tickets_by_category((int)$user['user_id'], $filterValue[0], $order_by, $search, $desc, '0');
        } else if ($by == 'P') {
            $inProgressTicketsCount = $this->LandlordModel->filter_inprogress_tickets_by_priority((int)$user['user_id'], $filterValue[0], $order_by, $search, $desc, '999');
            $result['pageCount2']   = ceil(count($inProgressTicketsCount)/3);
            $result['data']         = $this->LandlordModel->filter_inprogress_tickets_by_priority((int)$user['user_id'], $filterValue[0], $order_by, $search, $desc, '0');
        }

        print (json_encode($result));

    }

    function pagination_unallocated_tickets(){

        $user           = $this->session->userdata('user');
        $first          = date('Y-m-d', time() - 2764800);
        $last           = date('Y-m-d', time() + 86400);
        $value          = $this->input->post('inputval');
        $page           = $this->input->post('page');
        $order_by       = check_null($this->input->post('orderBy'));
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));

        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if ($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $unallocatedTicketsCount = $this->LandlordModel->pagination_unallocated_tickets((int)$user['user_id'], $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, 999);
        $result['pageCount']     = ceil(count($unallocatedTicketsCount)/3);
        $result['data']          = $this->LandlordModel->pagination_unallocated_tickets((int)$user['user_id'], $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, $page);

        print (json_encode($result));

    }

    function pagination_inprogress_tickets(){

        $user = $this->session->userdata('user');

        $value          = $this->input->post('inputval');
        $page           = $this->input->post('page');
        $order_by       = check_null($this->input->post('orderBy'));
        $desc           = check_null($this->input->post('desc'));
        $filterVal      = check_null($this->input->post('filterValue'));
        $filterCategory = check_null($this->input->post('filterCategory'));

        if ($filterCategory == 'C' && $filterVal != '') {
            $filterWhere = 'ticket_category_id';
        } else if ($filterCategory == 'P' && $filterVal != '') {
            $filterWhere = 'tickets.priority';
        } else {
            $filterWhere = '';
        }

        $inProgressTicketsCount = $this->LandlordModel->pagination_inprogress_tickets((int)$user['user_id'], $value, $order_by, $desc, $filterVal, $filterWhere, 999);
        $result['pageCount2']   = ceil(count($inProgressTicketsCount)/3);
        $result['data']         = $this->LandlordModel->pagination_inprogress_tickets((int)$user['user_id'], $value, $order_by, $desc, $filterVal, $filterWhere, $page);

        print (json_encode($result));
    }

    function tickets($ticket_id){

        check_int($ticket_id);
        check_landlord_login();

        $user   = $this->session->userdata('user');
        $result = $this->LandlordModel->get_ticket($ticket_id);

        $landlordTeamMembers = $this->LandlordModel->get_team_members((int)$user['user_id']);

        if($result ){
            $this->data['ticketInformation'] = $result;
            $this->data['landlordTeam']      = $landlordTeamMembers;

        } else {
            show_error('Page you are trying to see is not available.');
        }

        $this->landlord_view('tickets');
    }


    function search_recently_rasied_tickets(){

        $user   = $this->session->userdata('user');
        $first  = date('Y-m-d', time() - 2764800);
        $last   = date('Y-m-d', time() + 86400);
        $value  = $this->input->post('inputval');
        $result = $this->LandlordModel->search_recently_rasied_tickets((int)$user['user_id'], $first, $last, $value);

        print (json_encode($result));

    }

    function search_oldest_unresolved_tickets(){

        $user   = $this->session->userdata('user');
        $first  = date('Y-m-d', time() - 2764800);
        $last   = date('Y-m-d', time() + 86400);
        $value  = $this->input->post('inputval');
        $result = $this->LandlordModel->search_oldest_unresolved_tickets((int)$user['user_id'], $first, $last, $value);

        print (json_encode($result));

    }

    function sort_recently_rasied_tickets(){

        $user  = $this->session->userdata('user');
        $first = date('Y-m-d', time() - 2764800);
        $last  = date('Y-m-d', time() + 86400);

        $order_by = $this->input->post('orderBy');
        $search   = check_null($this->input->post('searchVal'),'0');
        $desc     = check_null($this->input->post('desc'));

        $result = $this->LandlordModel->sort_recently_rasied_tickets((int)$user['user_id'], $first, $last, $order_by, $search, $desc);

        print (json_encode($result));

    }

    function sort_oldest_unresolved_tickets(){

        $user  = $this->session->userdata('user');
        $first = date('Y-m-d', time() - 2764800);
        $last  = date('Y-m-d', time() + 86400);

        $order_by = $this->input->post('orderBy');
        $search   = check_null($this->input->post('searchVal'),'0');
        $desc     = check_null($this->input->post('desc'));

        $result = $this->LandlordModel->sort_oldest_unresolved_tickets((int)$user['user_id'], $first, $last, $order_by, $search, $desc);

        print (json_encode($result));

    }

    function add_team_member(){
        $this->load->view('landlord/addnewteammember');
    }

    function send_to_team_member(){

        if ($this->input->server('REQUEST_METHOD') === 'POST') {

            $post           = $this->input->post('member');
            $ticket_id      = (int) explode(".",$post)[1];
            $team_member_id = (int) explode(".",$post)[0];

            $data['ticket_id']     = $ticket_id;
            $data['contractor_id'] = $team_member_id;

            $result = $this->LandlordModel->send_ticket_to_team_member($data);

            if ($result) {

                $pusher          = $this->ci_pusher->get_pusher();
                $data['message'] = 'This message was sent at ' . date('Y-m-d H:i:s');
                $event           = $pusher->trigger('test_channel', 'my_event', $data);

                $this->session->set_userdata('comment', $comment);

            } else {

            }

            redirect('landlord/tickets/' . $ticket_id);

        } else {
            redirect('landlord');
        }
    }

    function editLandlordDescription(){

        $newDescription = $this->input->post('newDesc');
        $ticket_id      = $this->input->post('ticketID');

        $this->LandlordModel->updateDescription($newDescription, $ticket_id);

        redirect('landlord/tickets/' . $ticket_id);
    }
}