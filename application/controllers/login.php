<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	function __construct(){
		parent::__construct();
        $this->load->model('UserModel');

        if($this->session->userdata('landlord'))
            redirect('landlord');

        if($this->session->userdata('tenant'))
            redirect('tenant');

        if($this->session->userdata('contractor'))
            redirect('contractor');

	}

	function index(){
		$this->load->view('login');
	}

	function signin(){

		$email = $this->input->post('email');
		$pass  = $this->input->post('pass');

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('pass', 'Password', 'required');

		if ($this->form_validation->run() == FALSE) {
			$this->session->set_flashdata('error', validation_errors());
			redirect('login');

		} else {
			if ($email && $pass) {

                $data['email'] = $email;
				$result        = $this->UserModel->get_login_users($data);

				if ($result) {
					$registered = (int)$result['registered'];

					if ($registered == 1) {
						if (password_verify($pass, $result['password'])) {
							$userType = (int)$result['user_type'];

							if ($userType == 1) {//The user is Landlord
								$this->session->set_userdata('landlord', $result);
								redirect('landlord');

							} else if($userType == 2) {//The user is Tenant
								$this->session->set_userdata('tenant', $result);
								redirect('tenant');

							} else if ($userType == 3) {//The user is Contractor
								$this->session->set_userdata('contractor', $result);
								redirect('contractor');
							}
						} else {
							$this->session->set_flashdata('error', 'The password is wrong');
							redirect('login');
						}
					} else if($registered == 0) {
						$this->session->set_flashdata('error', 'Sorry you don`t verify your email');
						redirect('login');
					}
				} else {
					$this->session->set_flashdata('error', 'The password and email is wrong');
					redirect('login');
				}
			} else {
				redirect(base_url() . 'login', 'refresh');
			}
		}
	}
}