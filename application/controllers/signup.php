<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller {
	function index(){
		$this->load->view('signup');
	}

	function reg(){
		$userType = (int)$_POST['regUserType'];
		$firstName = $_POST['first_name'];
		$lastName = $_POST['last_name'];
		$email = $_POST['email'];
		$address = $_POST['address'];
		$pass = $_POST['pass'];
		$pass_confirm = $_POST['pass_confirm'];
		$pass_hash = password_hash($pass, PASSWORD_DEFAULT);
		$phone = $_POST['phone_number'];
		$now = date('Y-m-d H:i:s');

		$this->form_validation->set_rules('first_name', 'First Name', 'trim|required|min_length[3]|max_length[12]');
		$this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|min_length[3]|max_length[12]');
		$this->form_validation->set_rules('pass', 'Password', 'trim|required|min_length[8]');
		$this->form_validation->set_rules('pass_confirm', 'Password Confirmation', 'trim|required|matches[pass]');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');

		if($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('error', validation_errors());
			redirect('signup');
		}else{
			$this->load->model('UserModel');
			$result = $this->UserModel->get_login_users(array(
					'email' => $email
				)
			);
			if(empty($result) || !$result){
				$email_code = md5(time() + 3600) . md5($pass_hash);
				$this->UserModel->insert(array(
						'first_name' => $firstName,
						'last_name' => $lastName,
						'email' => $email,
						'address' => $address,
						'password' => $pass_hash,
						'phone' => $phone,
						'user_type' => $userType,
						'registered_at' =>$now,
						'email_code' => $email_code
					)
				);
				$userID = $this->UserModel->get_login_users(array(
						'email' => $email
					)
				);
				$user_id = $userID[0]['user_id'];
				$email_verify_link = base_url() . 'emailverify/verify/' . $email_code . '/' . $user_id;
				$config['protocol']    = 'smtp';
		        $config['smtp_host']    = 'ssl://smtp.gmail.com';
		        $config['smtp_port']    = '465';
		        $config['smtp_timeout'] = '7';
		        $config['smtp_user']    = 'narinekhachatryan.19@gmail.com';
		        $config['smtp_pass']    = 'nKh1217@E1';
		        $config['charset']    = 'utf-8';
		        $config['newline']    = "\r\n";
		        $config['mailtype'] = 'html'; // or html
		        $config['validation'] = TRUE;

		        $this->email->initialize($config);

		        $message = '<h4>Hi Dear ' . $firstName . ' ' . $lastName . '</h4>';
		        $message .= "<p>Please click the <a href='" . $email_verify_link . "'>link</a> from verify your email</p>";

		        $this->email->from('narinekhachatryan.19@gmail.com', 'Narine Khachatryan');
		        $this->email->to($email);
		        $this->email->subject('Email Verification');
		        $this->email->message($message);
		        $this->email->send();
		        $this->session->set_flashdata('success_email_message_send', 'We send in your email code from verify your profile, Please confirm :)');
		        redirect('login');
			}else{
				$this->session->set_flashdata('error', 'This email already exists');
				redirect('signup');
			}
		}

	}
}