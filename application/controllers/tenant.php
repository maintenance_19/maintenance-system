<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tenant extends CI_Controller {
	function index(){
		$user = $this->session->userdata('user');
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$this->load->model('TenantModel');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$current_tickets = $this->TenantModel->get_current_tickets((int)$landlord_id, $first, $last, '0');
		$resolved_tickets = $this->TenantModel->get_resolved_tickets((int)$landlord_id, $first, $last, '0');
		$current_tickets_count = $this->TenantModel->get_current_tickets((int)$landlord_id, $first, $last);
		$resolved_tickets_count = $this->TenantModel->get_resolved_tickets((int)$landlord_id, $first, $last);
		$data['user'] = $user;
		$data['current_tickets'] = $current_tickets;
		$data['resolved_tickets'] = $resolved_tickets;
		$data['current_tickets_pages_count'] = ceil(count($current_tickets_count)/3);
		$data['resolved_tickets_pages_count'] = ceil(count($resolved_tickets_count)/3);
		$this->load->view('tenantProfile', array('data' => $data));
	}

	function search_current_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$value = $_POST['inputval'];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$current_tickets_count = $this->TenantModel->search_current_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '999');
		$result['pageCount'] = ceil(count($current_tickets_count)/3);
		$result['data'] = $this->TenantModel->search_current_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '0');
		$result = json_encode($result);
		print $result;
	}

	function search_resolved_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$value = $_POST['inputval'];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$resolved_tickets_count = $this->TenantModel->search_resolved_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '999');
		$result['pageCount'] = ceil(count($resolved_tickets_count)/3);
		$result['data'] = $this->TenantModel->search_resolved_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, '0');
		$result = json_encode($result);
		print $result;
	}

	function sort_current_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$search = isset($_POST['searchVal']);
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$search){
			$search = '0';
		}else{
			$search = $_POST['searchVal'];
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$current_tickets_count = $this->TenantModel->sort_current_tickets((int)$landlord_id, $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '999');
		$result['pageCount'] = ceil(count($current_tickets_count)/3);
		$result['data'] = $this->TenantModel->sort_current_tickets((int)$landlord_id, $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '0');
		$result = json_encode($result);
		print $result;
	}

	function sort_resolved_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$search = isset($_POST['searchVal']);
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$search){
			$search = '0';
		}else{
			$search = $_POST['searchVal'];
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$resolved_tickets_count = $this->TenantModel->sort_resolved_tickets((int)$landlord_id, $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '999');
		$result['pageCount'] = ceil(count($resolved_tickets_count)/3);
		$result['data'] = $this->TenantModel->sort_resolved_tickets((int)$landlord_id, $first, $last, $order_by, $search, $desc, $filterVal, $filterWhere, '0');
		$result = json_encode($result);
		print $result;
	}

	function mark_as_resolved(){
		$this->load->model('TenantModel');
		$this->TenantModel->resolved_mark();
	}

	function reportingarepair(){
		$user = $this->session->userdata('user');
		$this->load->model('TenantModel');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$landlord = $this->TenantModel->get_landlord_information((int)$landlord_id);
		$data['user'] = $user;
		$data['landlord'] = $landlord[0];
		$this->load->view('tenant/reportingarepair', array('data' => $data));
	}

	function add_ticket(){
		$pathToUpload = '././assets/uploads/img';
		if(!file_exists($pathToUpload)){
			$create = mkdir($pathToUpload);
			if(!$create){
				return;
			}
		}

		$photos = [];

		foreach ($_FILES as $key => $value) {
			if($value['name'] == ""){
				continue;
			}else{
				$this->load->library('upload');
			    $imgName= uniqid();
			    $config['upload_path'] = $pathToUpload;
			    $config['allowed_types'] = 'gif|jpg|png';
			    $config['max_size'] = '9999';
			    $config['file_name'] = $imgName . '.jpg';

			    $photos[] = $imgName;

			    $this->upload->initialize($config);
			    $upload = $this->upload->do_upload($key);
			}
		}

		$photos = json_encode($photos);

		$user = $this->session->userdata('user');
		$this->load->model('UserModel');
		$this->load->model('TenantModel');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$now = date('Y-m-d H:i:s');

		if(isset($_POST['emailConfirm'])){
			$newEmail = $_POST['emailConfirm'];
			$this->UserModel->userDetailUpdate((int)$user['user_id'], $newEmail, 'email');
		}
		if(isset($_POST['phoneConfirm'])){
			$newPhoneNumber = $_POST['phoneConfirm'];
			$this->UserModel->userDetailUpdate((int)$user['user_id'], $newPhoneNumber, 'phone');
		}
		if(isset($_POST['addressConfirm'])){
			$newAddress = $_POST['addressConfirm'];
			$this->UserModel->userDetailUpdate((int)$user['user_id'], $newAddress, 'address');
		}

		$problem_id = $_POST['problemCategory'];
		$category_id = $_POST['ticketCategory'];
		$mark_id = $_POST['mark'];

		if(strlen($_POST['problemDescription']) > 0){
			$problemDescription = $_POST['problemDescription'];
		}

		if(strlen($_POST['briefDescription']) > 0){
			$briefDescription = $_POST['briefDescription'];
		}

		$data = array(
			'landlord_id' => (int)$landlord_id,
			'repair_problem_id' => (int)$problem_id,
			'ticket_category_id' => (int)$category_id,
			'tenant_description' => $problemDescription,
			'tenant_short_description' => $briefDescription,
			'marks_id' => (int)$mark_id,
			'created_date' => $now,
			'photos' => $photos
		);

		$this->TenantModel->add_new_ticket_by_tenant($data);
		redirect('tenant');
	}

	function filterCurrentTickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$filterValue = $_POST['filterValue'];
		$by = $filterValue[1];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$search = isset($_POST['searchVal']);
		$desc = isset($_POST['desc']);

		if(!$search){
			$search = '0';
		}else{
			$search = $_POST['searchVal'];
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		if($by == 'C'){
			$unallocatedTicketsCount = $this->TenantModel->filterCurrentTicketsByCategory((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
			$result['pageCount'] = ceil(count($unallocatedTicketsCount)/3);
			$result['data'] = $this->TenantModel->filterCurrentTicketsByCategory((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
		}else if($by == 'P'){
			$unallocatedTicketsCount = $this->TenantModel->filterCurrentTicketsByPriority((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
			$result['pageCount'] = ceil(count($unallocatedTicketsCount)/3);
			$result['data'] = $this->TenantModel->filterCurrentTicketsByPriority((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
		}
		$result = json_encode($result);
		print $result;
	}

	function filterResolvedTickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$filterValue = $_POST['filterValue'];
		$by = $filterValue[1];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$search = isset($_POST['searchVal']);
		$desc = isset($_POST['desc']);

		if(!$search){
			$search = '0';
		}else{
			$search = $_POST['searchVal'];
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		if($by == 'C'){
			$unallocatedTicketsCount = $this->TenantModel->filterResolvedTicketsByCategory((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
			$result['pageCount'] = ceil(count($unallocatedTicketsCount)/3);
			$result['data'] = $this->TenantModel->filterResolvedTicketsByCategory((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
		}else if($by == 'P'){
			$unallocatedTicketsCount = $this->TenantModel->filterResolvedTicketsByPriority((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '999');
			$result['pageCount'] = ceil(count($unallocatedTicketsCount)/3);
			$result['data'] = $this->TenantModel->filterResolvedTicketsByPriority((int)$landlord_id, $first, $last, $filterValue[0], $order_by, $search, $desc, '0');
		}
		$result = json_encode($result);
		print $result;
	}

	function editTenantDescription(){
		$newDescription = $_POST['newDesc'];
		$ticket_id = $_POST['ticketID'];
		$this->load->model('TenantModel');
		$this->TenantModel->updateDescription($newDescription, $ticket_id);
		redirect('tenant/tickets/' . $ticket_id);
	}

	function addNewProblemPhoto(){
		$this->load->model('TenantModel');
		
		$ticket_id = $_POST['ticketID'];

		$ticketPhotos = $this->TenantModel->get_ticket_photos($ticket_id)[0]['photos'];
		$ticketPhotos = json_decode($ticketPhotos);

		$pathToUpload = '././assets/uploads/img';
		if(!file_exists($pathToUpload)){
			$create = mkdir($pathToUpload);
			if(!$create){
				return;
			}
		}

		$value = current((array)$_FILES);
		$key = key((array)$_FILES);

		if($value['name'] == ""){
			redirect('tenant/tickets/' . $ticket_id);
		}else{
			$this->load->library('upload');
			$imgName= uniqid();
			$config['upload_path'] = $pathToUpload;
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '9999';
			$config['file_name'] = $imgName . '.jpg';

			$this->upload->initialize($config);
			$upload = $this->upload->do_upload($key);
		}

		$ticketPhotos[] = $imgName;

		$ticketPhotos = json_encode($ticketPhotos);
		$this->TenantModel->update_ticket_photos($ticket_id, $ticketPhotos);

		redirect('tenant/tickets/' . $ticket_id);
	}

	function pagination_current_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$value = $_POST['inputval'];
		$page = $_POST['page'];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$pagination_current_tickets_count = $this->TenantModel->pagination_current_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, 999);
		$result['pageCount'] = ceil(count($pagination_current_tickets_count)/3);
		$result['data'] = $this->TenantModel->pagination_current_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, $page);
		$result = json_encode($result);
		print $result;
	}

	function pagination_resolved_tickets(){
		$this->load->model('TenantModel');
		$user = $this->session->userdata('user');
		$landlord_id = $this->TenantModel->get_landlord_id((int)$user['user_id'])[0]['landlord_id'];
		$first = date('Y-m-d', time() - 2764800);
		$last = date('Y-m-d', time() + 86400);
		$value = $_POST['inputval'];
		$page = $_POST['page'];
		if(isset($_POST['orderBy'])){
			$order_by = $_POST['orderBy'];
		}else{
			$order_by = '';
		}
		$desc = isset($_POST['desc']);
		$filterVal = isset($_POST['filterValue']);
		$filterCategory = isset($_POST['filterCategory']);

		if(!$filterVal){
			$filterVal = '';
		}else{
			$filterVal = $_POST['filterValue'];
		}

		if(!$filterCategory){
			$filterCategory = '';
		}else{
			$filterCategory = $_POST['filterCategory'];
		}

		if($filterCategory == 'C' && $filterVal != ''){
			$filterWhere = 'ticket_category_id';
		}else if($filterCategory == 'P' && $filterVal != ''){
			$filterWhere = 'tickets.priority';
		}else{
			$filterWhere = '';
		}

		if(!$desc){
			$desc = '';
		}else{
			$desc = $_POST['desc'];
		}

		$pagination_resolved_tickets_count = $this->TenantModel->pagination_resolved_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, 999);
		$result['pageCount'] = ceil(count($pagination_resolved_tickets_count)/3);
		$result['data'] = $this->TenantModel->pagination_resolved_tickets((int)$landlord_id, $first, $last, $value, $order_by, $desc, $filterVal, $filterWhere, $page);
		$result = json_encode($result);
		print $result;	
	}

	function tickets($ticket_id){
		$this->load->model('TenantModel');
		$result = $this->TenantModel->get_ticket($ticket_id);
		$data['ticketInformation'] = $result;
		$this->load->view('tenant/tickets', array('tickets' => $data));
	}
}