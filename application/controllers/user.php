<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	function logOut(){
		session_destroy();
		redirect('login');
	}
}
?>