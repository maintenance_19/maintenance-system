<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

	public $data = [];

	function __construct()
	{
		parent::__construct();
	}

	public function landlord_view($content_view) 
	{
		$this->load->view(config_item('landlord_dir').'header', $this->data);
		$this->load->view(config_item('landlord_dir') . $content_view  );
		$this->load->view(config_item('landlord_dir').'footer');

    }

	public function contractor_view($content_view)
	{
		$this->load->view(config_item('contractor_dir').'header', $this->data);
		$this->load->view(config_item('contractor_dir') . $content_view);
		$this->load->view(config_item('contractor_dir').'footer');

    }

}
