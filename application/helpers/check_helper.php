<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function check_int($number){
		if(!is_numeric($number)) show_error('Page you are trying to see is not available.');
	}	

	function check_landlord_login(){

		$CI =& get_instance();
		if(!$CI->session->userdata('landlord'))
			redirect('login');

	}

	function check_tenant_login(){

		$CI =& get_instance();
		if(!$CI->session->userdata('tenant'))
			redirect('login');

	}

	function check_contractor_login(){

		$CI =& get_instance();
		if(!$CI->session->userdata('contractor'))
			redirect('login');

	}

	function check_null($value = null , $result = ''){

		if(!is_null($value)){
			return $value ;
		} else {
			return $result;
		}
	}