<?php 
	class ContractorModel extends CI_Model {
		private $tablename = 'tickets';

		function get_landlord_id($id){
			$this->db->select('landlord_id');
			$this->db->from('landlord_team');
			$this->db->where('contractor_id', $id);
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_outstanding_tickets($id, $offset = '999'){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function search_outstanding_tickets($id, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where($where);
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function sort_outstanding_tickets($id, $orderBy, $searchVal, $desc, $filterVal, $filterWhere, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filter_outstanding_tickets_by_category($id, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('tickets.ticket_category_id', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filter_outstanding_tickets_by_priority($id, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('tickets.priority', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function pagination_outstanding_tickets($id, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			if($val != '0'){
				$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != 999){
				if($offset == 1){
					$offset = 0;
				}else if($offset == 2){
					$offset = 3;
				}else{
					$offset = $offset + ($offset-1) + ($offset - 2);
				}
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		// function get_count_unresolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(contractor_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('contractor_id', $id);
		// 	$this->db->where('resolved_date IS NULL');
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_count_resolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(contractor_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('contractor_id', $id);
		// 	$this->db->where('resolved_date IS NOT NULL');
		// 	$this->db->where('resolved_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_total_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(contractor_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('contractor_id', $id);
		// 	$this->db->where('contractor_id IS NOT NULL');
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;	
		// }

		// function get_recently_rasied_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('contractor_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_oldest_unresolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('contractor_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->where('resolved_date IS NULL');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }
	}
?>