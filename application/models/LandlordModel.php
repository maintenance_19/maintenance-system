<?php 
	class LandlordModel extends CI_Model {
		private $tablename = 'tickets';

		function get_unallocated_tickets_count($id, $firstdate, $lastdate){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			$result = $this->db->get()->result_array();
			return $result;			
		}

		function get_unallocated_tickets($id, $firstdate, $lastdate, $offset){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			$this->db->limit(3, $offset);
			$result = $this->db->get()->result_array();
			return $result;			
		}

		function get_inprogress_tickets($id, $offset = '999'){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;	
		}

		function search_unallocated_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			$this->db->where($where);
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function search_inprogress_tickets($id, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			$this->db->where($where);
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function pagination_unallocated_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			if($val != '0'){
				$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != 999){
				if($offset == 1){
					$offset = 0;
				}else if($offset == 2){
					$offset = 3;
				}else{
					$offset = $offset + ($offset-1) + ($offset - 2);
				}
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function pagination_inprogress_tickets($id, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			if($val != '0'){
				$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != 999){
				if($offset == 1){
					$offset = 0;
				}else if($offset == 2){
					$offset = 3;
				}else{
					$offset = $offset + ($offset-1) + ($offset - 2);
				}
				$this->db->limit(3, $offset);
			}	
			$result = $this->db->get()->result_array();
			return $result;
		}

		function sort_unallocated_tickets($id, $firstdate, $lastdate, $orderBy, $searchVal, $desc, $filterVal, $filterWhere, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function sort_inprogress_tickets($id, $orderBy, $searchVal, $desc, $filterVal, $filterWhere, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;	
		}

		function filter_unallocated_tickets_by_category($id, $firstdate, $lastdate, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('tickets.ticket_category_id', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filter_inprogress_tickets_by_category($id, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->where('tickets.ticket_category_id', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filter_unallocated_tickets_by_priority($id, $firstdate, $lastdate, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('tickets.priority', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filter_inprogress_tickets_by_priority($id, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('ticket_contractors.contractor_id IS NOT NULL');
			$this->db->where('tickets.priority', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_ticket($id){
			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date, tickets.created_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $id);
			$this->db->join('users', 'tickets.landlord_id = users.user_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query1 = $this->db->get_compiled_select();

			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date, tickets.created_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $id);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query2 = $this->db->get_compiled_select();

			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date, tickets.created_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $id);
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_contractors.contractor_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query3 = $this->db->get_compiled_select();

			$query = $this->db->query($query1 . ' UNION ' . $query2 . ' UNION ' . $query3);
			$result = $query->result_array();
			return $result;
		}

		function send_ticket_to_team_member($array){
			return	$this->db->insert('ticket_contractors', $array);
		}

		function updateDescription($text, $ticket_id){
			$this->db->set('landlord_description', $text);
			$this->db->where('ticket_id', $ticket_id);
			$this->db->update($this->tablename);
		}

		// function get_count_unresolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(landlord_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('resolved_date IS NULL');
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_count_resolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(landlord_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('resolved_date IS NOT NULL');
		// 	$this->db->where('resolved_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_total_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('count(landlord_id)');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('contractor_id IS NOT NULL');
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;	
		// }

		// function get_recently_rasied_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function get_oldest_unresolved_tickets($id, $firstdate, $lastdate){
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->where('resolved_date IS NULL');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		function get_team_members($id){
			$this->db->select('users.user_id, users.first_name, users.last_name, users.email, users.phone, users.photo, users.address');
			$this->db->from('landlord_team');
			$this->db->where('landlord_id', $id);
			$this->db->join('users', 'users.user_id = landlord_team.contractor_id');
			$result = $this->db->get()->result_array();
			return $result;
		}

		function change_ticket_priority($id, $priority){
			$this->db->set('priority', $priority);
			$this->db->where('ticket_id', $id);
			$this->db->update($this->tablename);
		}

		// function search_recently_rasied_tickets($id, $firstdate, $lastdate, $val){
		// 	$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where($where);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function search_oldest_unresolved_tickets($id, $firstdate, $lastdate, $val){
		// 	$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or repair_problems.problem_name LIKE '%$val%')";
		// 	$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 	$this->db->from($this->tablename);
		// 	$this->db->where($where);
		// 	$this->db->where('landlord_id', $id);
		// 	$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 	$this->db->where('resolved_date IS NULL');
		// 	$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 	$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 	$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 	$result = $this->db->get()->result_array();
		// 	return $result;
		// }

		// function sort_recently_rasied_tickets($id, $firstdate, $lastdate, $orderBy, $searchVal, $desc){
		// 	if($searchVal == '0'){
		// 		$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 		$this->db->from($this->tablename);
		// 		$this->db->where('landlord_id', $id);
		// 		$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 		$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 		$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 		$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 		$this->db->order_by($orderBy, $desc);
		// 		$result = $this->db->get()->result_array();
		// 		return $result;
		// 	}else{
		// 		$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
		// 		$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 		$this->db->from($this->tablename);
		// 		$this->db->where($where);
		// 		$this->db->where('landlord_id', $id);
		// 		$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 		$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 		$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 		$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 		$this->db->order_by($orderBy, $desc);
		// 		$result = $this->db->get()->result_array();
		// 		return $result;
		// 	}
		// }

		// function sort_oldest_unresolved_tickets($id, $firstdate, $lastdate, $orderBy, $searchVal, $desc){
		// 	if($searchVal == '0'){
		// 		$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 		$this->db->from($this->tablename);
		// 		$this->db->where('landlord_id', $id);
		// 		$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 		$this->db->where('resolved_date IS NULL');
		// 		$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 		$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 		$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 		$this->db->order_by($orderBy, $desc);
		// 		$result = $this->db->get()->result_array();
		// 		return $result;
		// 	}else{
		// 		$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or repair_problems.problem_name LIKE '%$searchVal%')";
		// 		$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name');
		// 		$this->db->from($this->tablename);
		// 		$this->db->where($where);
		// 		$this->db->where('landlord_id', $id);
		// 		$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
		// 		$this->db->where('resolved_date IS NULL');
		// 		$this->db->join('users', 'tickets.tenant_id = users.user_id');
		// 		$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id');
		// 		$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id');
		// 		$this->db->order_by($orderBy, $desc);
		// 		$result = $this->db->get()->result_array();
		// 		return $result;
		// 	}
		// }
	}
?>