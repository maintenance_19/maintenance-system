<?php 
	class TenantModel extends CI_Model {
		private $tablename = 'tickets';

		function get_landlord_id($id){
			$this->db->select('landlord_id');
			$this->db->from('landlord_tenant');
			$this->db->where('tenant_id', $id);
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_current_tickets($id, $firstdate, $lastdate, $offset = '999'){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_resolved_tickets($id, $firstdate, $lastdate, $offset = '999'){
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function search_current_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or ticket_category.category LIKE '%$val%')";
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where($where);
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function search_resolved_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or ticket_category.category LIKE '%$val%')";
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			$this->db->where($where);
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function sort_current_tickets($id, $firstdate, $lastdate, $orderBy, $searchVal, $desc, $filterVal, $filterWhere, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function sort_resolved_tickets($id, $firstdate, $lastdate, $orderBy, $searchVal, $desc, $filterVal, $filterWhere){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_landlord_information($landlord_id){
			$this->db->select('*');
			$this->db->from('users');
			$this->db->where('user_id', $landlord_id);
			$result = $this->db->get()->result_array();
			return $result;
		}

		function add_new_ticket_by_tenant($array){
			$this->db->insert($this->tablename, $array);
		}

		function filterCurrentTicketsByCategory($id, $firstdate, $lastdate, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->where('ticket_category_id', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filterCurrentTicketsByPriority($id, $firstdate, $lastdate, $priority, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->where('tickets.priority', $priority);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filterResolvedTicketsByCategory($id, $firstdate, $lastdate, $category, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('ticket_category_id', $category);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function filterResolvedTicketsByPriority($id, $firstdate, $lastdate, $priority, $orderBy, $searchVal, $desc, $offset){
			if($searchVal != '0'){
				$where = "(users.address LIKE '%$searchVal%' or users.first_name LIKE '%$searchVal%' or users.last_name LIKE '%$searchVal%' or ticket_category.category LIKE '%$searchVal%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('tickets.priority', $priority);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($orderBy != '' && $desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else if($orderBy != '' && $desc == ''){
				$this->db->order_by($orderBy);
			}
			if($offset != '999'){
				$this->db->limit(3, $offset);
			}
			$result = $this->db->get()->result_array();
			return $result;
		}

		function get_ticket($ticket_id){
			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $ticket_id);
			$this->db->join('users', 'tickets.landlord_id = users.user_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query1 = $this->db->get_compiled_select();

			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $ticket_id);
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query2 = $this->db->get_compiled_select();

			$this->db->select('tickets.ticket_id, tickets.created_date, users.address, ticket_category.category, users.first_name, users.last_name, users.phone, users.email, users.photo, users.user_type, marks.mark, contractor_professions.name, tickets.tenant_description, tickets.tenant_short_description, tickets.contractor_description, tickets.landlord_description, tickets.photos, tickets.complete_date');
			$this->db->from($this->tablename);
			$this->db->where('tickets.ticket_id', $ticket_id);
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_contractors.contractor_id', 'left');
			$this->db->join('contractor_professions', 'users.contractor_profession_id = contractor_professions.id', 'left');
			$this->db->join('marks', 'tickets.marks_id = marks.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			$query3 = $this->db->get_compiled_select();

			$query = $this->db->query($query1 . ' UNION ' . $query2 . ' UNION ' . $query3);
			$result = $query->result_array();
			return $result;
		}

		function pagination_current_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			if($val != '0'){
				$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or ticket_category.category LIKE '%$val%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->where('complete_date IS NULL');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != 999){
				if($offset == 1){
					$offset = 0;
				}else if($offset == 2){
					$offset = 3;
				}else{
					$offset = $offset + ($offset-1) + ($offset - 2);
				}
				$this->db->limit(3, $offset);
			}	
			$result = $this->db->get()->result_array();
			return $result;
		}

		function pagination_resolved_tickets($id, $firstdate, $lastdate, $val, $orderBy, $desc, $filterVal, $filterWhere, $offset){
			if($val != '0'){
				$where = "(users.address LIKE '%$val%' or users.first_name LIKE '%$val%' or users.last_name LIKE '%$val%' or ticket_category.category LIKE '%$val%')";
			}
			$this->db->select('tickets.ticket_id, users.first_name, users.last_name, users.photo, users.address, tickets.created_date, repair_problems.problem_name, priority_types.priority_name, ticket_status.ticket_status, ticket_category.category');
			$this->db->from($this->tablename);
			if(isset($where)){
				$this->db->where($where);
			}
			if($filterWhere != '' && $filterVal != ''){
				$this->db->where($filterWhere, $filterVal);
			}
			$this->db->where('landlord_id', $id);
			$this->db->where('resolved_date IS NOT NULL');
			$this->db->where('complete_date IS NOT NULL');
			$this->db->where('created_date BETWEEN "' . $firstdate . '" ' . 'AND' . ' "' . $lastdate . '"');
			$this->db->join('ticket_tenants', 'tickets.ticket_id = ticket_tenants.ticket_id', 'left');
			$this->db->join('ticket_contractors', 'tickets.ticket_id = ticket_contractors.ticket_id', 'left');
			$this->db->join('users', 'users.user_id = ticket_tenants.tenant_id', 'left');
			$this->db->join('repair_problems', 'tickets.repair_problem_id = repair_problems.repair_problem_id', 'left');
			$this->db->join('priority_types', 'tickets.priority = priority_types.priority_id', 'left');
			$this->db->join('ticket_status', 'tickets.status_id = ticket_status.id', 'left');
			$this->db->join('ticket_category', 'tickets.ticket_category_id = ticket_category.id', 'left');
			if($desc != ''){
				$this->db->order_by($orderBy, $desc);
			}else{
				$this->db->order_by($orderBy);
			}
			if($offset != 999){
				if($offset == 1){
					$offset = 0;
				}else if($offset == 2){
					$offset = 3;
				}else{
					$offset = $offset + ($offset-1) + ($offset - 2);
				}
				$this->db->limit(3, $offset);
			}	
			$result = $this->db->get()->result_array();
			return $result;
		}

		function updateDescription($text, $ticket_id){
			$this->db->set('tenant_description', $text);
			$this->db->where('ticket_id', $ticket_id);
			$this->db->update($this->tablename);
		}

		function get_ticket_photos($ticket_id){
			$this->db->select('photos');
			$this->db->from($this->tablename);
			$this->db->where('ticket_id', $ticket_id);
			$result = $this->db->get()->result_array();
			return $result;
		}

		function update_ticket_photos($ticket_id, $photos){
			$this->db->set('photos', $photos);
			$this->db->where('ticket_id', $ticket_id);
			$this->db->update($this->tablename);
		}
	}
?>