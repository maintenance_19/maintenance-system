<?php 
	class UserModel extends CI_Model {
		public $tablename = 'users';

		function get_login_users($array){
			$this->db->select('*');
			$this->db->from($this->tablename);
			$this->db->where('email', $array['email']);
			$result = $this->db->get()->row_array();
			return $result;
		}

		function insert($data){
			$this->db->insert($this->tablename, $data);
		}

		function update($data, $id){
			$this->db->where('user_id', $id);
			$this->db->update($this->tablename, $data);
		}

		function getEmailCode($array){
			$this->db->select('email_code');
			$this->db->from($this->tablename);
			$this->db->where('user_id', $array['user_id']);
			$result = $this->db->get()->result_array();
			return $result;
		}

		function userDetailUpdate($user_id, $new, $fieldName){
			$this->db->set($fieldName, $new);
			$this->db->where('user_id', $user_id);
			$this->db->update($this->tablename);
		}
	}
?>