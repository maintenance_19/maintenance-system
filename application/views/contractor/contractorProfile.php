<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Contractor Profile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//js.pusher.com/2.2/pusher.min.js" type="text/javascript"></script>
</head>
<body>
	<button style="background: green;color: white"><a href="<?php echo base_url() . 'user/logOut' ?>">Log Out</a></button>
	<h1 class="col-md-12 col-md-offset-4" style="font-family: gabriola;font-size: 50px">Contractor Dashboard</h1>
    <span id="tickets_notification_count"></span>
	<div class="container">
		<input id="searchOutstandingTickets" type="text" placeholder="Search">
		<select id="sortOutstandingTickets">
			<option selected="" disabled="">Sort</option>
			<option value="NF">Newest First</option>
			<option value="OF">Oldest First</option>
			<option value="HF">Highest Priority First</option>
			<option value="LF">Lowest Priority First</option>
		</select>
		<select class="selectpicker" id="filterOutstandingTickets">
			<option selected="" disabled="">Filter</option>
  			<optgroup label="ByCategory" data-max-options="2">
    			<option value="1C">Plumbing</option>
    			<option value="2C">Electrician</option>
    			<option value="3C">Maintenance</option>
    			<option value="4C">Carpentry</option>
    			<option value="5C">Gardening</option>
  			</optgroup>
  			<optgroup label="By Priority" data-max-options="2">
    			<option value="3P">High</option>
    			<option value="2P">Medium</option>
    			<option value="1P">Low</option>
  			</optgroup>
		</select>
		<div id="outstandingTickets" class="panel panel-default">
			<div class="panel-heading" style="font-size: 20px">Outstanding Tickets</div>
			<?php if ($outstandingTickets) { ?>
				<?php foreach ($outstandingTickets as $ticket) { ?>
					<?php
						$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
						$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
					?>
					<div class="col-md-12 panel panel-primary">
						<div class="col-md-6">
							<label>Tenant Name: </label>
							<p><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></p>
							<label>Photo: </label>
							<?php if(isset($ticket['photo'])){ ?>
								<img class="img-circle" src="<?php echo $ticket['photo'] ?>">
							<?php }else{ ?>
								<p>No Photo</p>
							<?php } ?>
							<label>Address: </label>
							<p><?php echo $ticket['address'] ?></p>
						</div>
						<div class="col-md-6" style="text-align: right">
							<label>Priority: </label>
							<p><?php echo $ticket['priority_name'] ?></p>
							<label>Issue: </label>
							<p><?php echo $ticket['problem_name'] ?> Issue</p>
							<label>Received on: </label>
							<p><?php echo $ticket['created_date'] ?></p>
						</div>
						<button class="viewOutstandingTickets btn btn-success"><a href="<?php echo base_url() . 'contractor/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>
					</div>
				<?php } ?>
				<button id="seeAllTickets" style="background: orange;color: black;border: 0;cursor: pointer;">See All</button>
				<?php if($pageCount){ ?>
					<ul class="pagination">
						<?php for($i=1;$i<=$pageCount;$i++) { ?>
							<li class="outstandingTicketsPagination"><a href="javascript: void(0)"><?php echo $i ?></a></li>	
						<?php } ?>
					</ul>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
<!-- 	<div>
		<h1>Analytics</h1>
		<h4>Unresolved Tickets Count: <?php echo $data['unresolved_tickets_count'][0]['count(contractor_id)'] ?></h4>
		<h4>Resolved Tickets Count: <?php echo $data['resolved_tickets_count'][0]['count(contractor_id)'] ?></h4>
		<h4>Total Tickets Count: <?php echo $data['total_tickets'][0]['count(contractor_id)'] ?></h4>
	</div><br><br>
	<div>
		<h1>Recently Rasied</h1>
		<?php foreach ($data['recently_rasied_tickets'] as $ticket) { ?>
			<?php
				$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
				$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
			?>
			<div>
				<img src="<?php echo $ticket['photo'] ?>">
				<h4><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></h4>
			</div>
			<div>
				<p><?php echo $ticket['priority_name'] ?></p>
			</div>
			<div>
				<h2><?php echo $ticket['address'] ?></h2>
			</div>
			<div>
				<h3><?php echo $ticket['problem_name'] ?> Issue</h3><br>
				<h4>Resolved on: <?php echo $ticket['created_date'] ?></h4>
			</div>
		<?php } ?>
	</div><br><br><br>
	<div>
		<h1>Oldest Unresolved</h1>
		<?php foreach ($data['oldest_unresolved_tickets'] as $ticket) { ?>
			<?php
				$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
				$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
			?>
			<div>
				<img src="<?php echo $ticket['photo'] ?>">
				<h4><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></h4>
			</div>
			<div>
				<p><?php echo $ticket['priority_name'] ?></p>
			</div>
			<div>
				<h2><?php echo $ticket['address'] ?></h2>
			</div>
			<div>
				<h3><?php echo $ticket['problem_name'] ?> Issue</h3><br>
				<h4>Resolved on: <?php echo $ticket['created_date'] ?></h4>
			</div>
		<?php } ?>
	</div> -->
</body>

    <script type="text/javascript">
        // Enable pusher logging - don't include this in production
        Pusher.log = function(message) {
            if (window.console && window.console.log) {
                window.console.log(message);
            }
        };

        $(document).ready(function(){

            var pusher = new Pusher('0a78f464a8b913990ae1');
            var channel = pusher.subscribe('test_channel');

            channel.bind('my_event', function(data) {
                //document.getElementById('event').innerHTML = data.message;

                var html  = '<div class="col-md-12 panel panel-primary">';
                    html += '<div class="col-md-6">';
                    html += '<label>Tenant Name: </label>';
                    html += '<p></p>';
                    html += '<label>Photo: </label>';
                    html += '<img class="img-circle" src="">';
                    html += '<p>No Photo</p>';
                    html += '<label>Address: </label>';
                    html += '<p></p>';
                    html += '</div>';
                    html += '<div class="col-md-6" style="text-align: right">';
                    html += '<label>Priority: </label>';
                    html += '<p></p>';
                    html += '<label>Issue: </label>';
                    html += '<p> Issue</p>';
                    html += '<label>Received on: </label>';
                    html += '<p></p>';
                    html += '</div>';
                    html += '<button class="viewOutstandingTickets btn btn-success"><a href="<?php echo base_url() . 'contractor/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>';
                    html += '</div>';

                $('#outstandingTickets .panel-heading').after($(html))
                console.log('data');
                console.log(data);
            });
        })
    </script>
	<script>
		$(document).ready(function(){
			let base_url = "<?php echo base_url() ?>";
			$('#searchOutstandingTickets').on('input', function(){
				let value = $(this).val().trim();
				let sortValue = $('#sortOutstandingTickets').val();
				let filterValue = $('#filterOutstandingTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'contractor/search_outstanding_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#outstandingTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Outstanding Tickets</div>");
							header.appendTo('#outstandingTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewOutstandingTickets btn btn-success'><a href='"+base_url+"contractor/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#outstandingTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='outstandingTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#outstandingTickets');
						}
					}
				})
			})

			$('#sortOutstandingTickets').on('change', function(){
				let sortValue = $(this).val();
				let searchValue = $('#searchOutstandingTickets').val().trim();
				let filterValue = $('#filterOutstandingTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'contractor/sort_outstanding_tickets',
					data: {
						'orderBy': order_by,
						'searchVal': searchValue,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#outstandingTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Outstanding Tickets</div>");
							header.appendTo('#outstandingTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewOutstandingTickets btn btn-success'><a href='"+base_url+"contractor/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#outstandingTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='outstandingTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#outstandingTickets');
						}
					}
				})
			})

			$('#filterOutstandingTickets').on('change', function(){
				let filterValue = $(this).val();
				let sortValue = $('#sortOutstandingTickets').val();
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				let searchValue = $('#searchOutstandingTickets').val().trim();

				$.ajax({
					type: 'post',
					url: base_url + 'contractor/filter_outstanding_tickets',
					data: {
						'filterValue': filterValue,
						'searchVal': searchValue,
						'orderBy': order_by,
						'desc': desc
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#outstandingTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Outstanding Tickets</div>");
							header.appendTo('#outstandingTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewOutstandingTickets btn btn-success'><a href='"+base_url+"contractor/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#outstandingTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='outstandingTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#outstandingTickets');
						}
					}
				})
			})

			$(document).on('click', '.outstandingTicketsPagination', function(){
				let pageValue = parseInt($(this).find('a').html());
				let value = $('#searchOutstandingTickets').val().trim();
				let sortValue = $('#sortOutstandingTickets').val();
				let filterValue = $('#filterOutstandingTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'contractor/pagination_outstanding_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal,
						'page': pageValue
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#outstandingTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Outstanding Tickets</div>");
							header.appendTo('#outstandingTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewOutstandingTickets btn btn-success'><a href='"+base_url+"contractor/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#outstandingTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='outstandingTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#outstandingTickets');
						}
					}
				})
			})
		})
	</script>
</html>