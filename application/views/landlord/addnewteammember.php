<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '1'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Landlord Team</title>
</head>
<body>
	<button style="background: green;color: white"><a href="<?php echo base_url() . 'user/logOut' ?>">Log Out</a></button>
	<br>
	<h1>Add Team Member</h1>
	<p>Add a member to your maintenance team and get repairs sent directly to them.Don't worry you'll still be notifed about repairs,but with the peace of mind that the right person already knows.</p>
	<div style="width: 100%;height: 500px;background: lightgreen">
		<div style="width: 50%;height: 100%;border: 1px dashed orange;background: lightblue;float: left">
			<h1 style="text-align: center;">Personal Details</h1>
			<input style="width: 100%;height: 50px;background: lightgreen;text-align: center;" type="text" name="name" placeholder="Full Name"><br>
			<input style="width: 100%;height: 50px;background: lightgreen;text-align: center;" type="text" name="email" placeholder="Email Address"><br>
			<input style="width: 100%;height: 50px;background: lightgreen;text-align: center;" type="text" name="phone" placeholder="Phone Number"><br>
			<select style="width: 100%;height: 50px">
				<option selected="" disabled="">Location</option>
			</select>
		</div>
		<div style="width: 49.7%;height: 100%;border: 1px dashed orange;background: lightblue;float: left">
			<h1 style="text-align: center;">Professional Details</h1>
			<p>Please select this team members occupation.This is how we determine who to send repair tickets to e.g. boiler's broken - plumber</p><br>
			<select style="width: 100%;height: 50px">
				<option selected="" disabled="">Occupation</option>
				<option>Plumber</option>
				<option>Electrician</option>
				<option>Labourer</option>
				<option>Manager</option>
				<option>Carpenter</option>
				<option>Gardener</option>
			</select>
			<input type="checkbox" name="preferred" checked="">Preffered Team Member<br>
			<input type="checkbox" name="sendAutomatically">Send tickets automatically <br><br>
			<button style="width: 50%;height: 50px;background: orange;border: 0;color: white;text-align: center;border-radius: 30%;margin-left: 25%">Invite Team Member</button>
		</div>
	</div>
</body>
</html>