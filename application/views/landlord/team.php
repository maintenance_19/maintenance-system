<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '1'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Landlord Team</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<button style="background: green;color: white"><a href="<?php echo base_url() . 'user/logOut' ?>">Log Out</a></button>
	<div class="panel-group">
		<?php foreach ($data['team_members'] as $k): ?>
			<div class="col-md-6 panel panel-success">
				<h1><?php echo $k['first_name'] . ' ' . $k['last_name'] ?></h1>
				<h5>Email: <?php echo $k['email'] ?></h5>
				<h4>Phone Number: <?php echo $k['phone'] ?></h4>
				<div>
					<img src="<?php echo $k['photo'] ?>" width="190" height="240">
				</div>
				<p>Adress: <?php echo $k['address'] ?></p>
			</div>
		<?php endforeach ?>
	</div>
</body>
</html>