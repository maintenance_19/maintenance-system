<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<?php if($ticketInformation){ ?>
				<div class="col-md-12 col-md-offset-3">
					<h1 style="font-family: gabriola">Repair Ticket (#<?php echo $ticketInformation[0]['ticket_id'] ?>)</h1><br>
				</div>
				<?php
				$ticketInformation[1]['created_date'] = explode(" ", $ticketInformation[1]['created_date'])[0];
				$ticketInformation[1]['created_date'] = date('d.m.Y', strtotime($ticketInformation[1]['created_date']));
				?>
				<h5 class="col-md-12 col-md-offset-3" style="font-family: cursive">Date received: <?php echo $ticketInformation[1]['created_date'] ?></h5>
				<h5 class="col-md-12 col-md-offset-3" style="font-family: cursive">Category: <?php echo $ticketInformation[0]['category'] ?></h5>
				<h1 style="font-family: gabriola">Reported By: </h1>
				<div class="col-md-12">
					<?php foreach ($ticketInformation as $tenant) { ?>
						<?php if($tenant['user_type'] == '2'){ ?>						
							<div class="col-md-6">
								<img src="<?php echo $tenant['photo'] ?>" class="img-circle">
								<h3><?php echo $tenant['first_name'] . ' ' . $tenant['last_name'] ?></h3>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-md-12 col-md-offset-3">
					<h1 style="font-family: gabriola">Problem Reported</h1>
					<h6><i><?php echo $ticketInformation[0]['tenant_short_description'] ?></i></h6>
					<p style="font-family: cursive" class="col-md-12 col-md-offset-1"><?php echo $ticketInformation[0]['tenant_description'] ?></p>
				</div>
				<div>
					<h1 style="font-family: gabriola" class="col-md-12 col-md-offset-3">Photographs</h1>
					<?php if($ticketInformation[0]['photos']){ ?>
						<?php
						$ticketInformation[0]['photos'] = json_decode($ticketInformation[0]['photos']);
						?>
						<?php foreach ($ticketInformation[0]['photos'] as $photo) { ?>
							<img class="img-thumbnail" width="200" height="200" src="<?php echo base_url() . 'assets/uploads/img/' . $photo . '.jpg' ?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="panel panel-primary">
					<div class="col-md-12">
						<div class="col-md-6">
							<h1>Contractors</h1>
						</div>
						<div class="col-md-6">
							<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#sendToContractor" style="border-radius: 50%">+</button>
						</div>
					</div>
					<h5>Notice: Marked as  '<?php echo $ticketInformation[0]['mark'] ?>'</h5>
					<div class="col-md-12">
						<?php foreach ($ticketInformation as $contractor) { ?>
							<?php if($contractor['user_type'] == '3'){ ?>						
								<div class="col-md-6">
									<img src="<?php echo $contractor['photo'] ?>" class="img-circle">
									<h3><?php echo $contractor['first_name'] . ' ' . $contractor['last_name'] ?></h3>
									<p><?php echo $contractor['name'] ?></p>
									<h3>Phone: <?php echo $contractor['phone'] ?></h3>
									<h3>Email: <?php echo $contractor['email'] ?></h3>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<div>
						<h1>Contractor Notes</h1>
						<p><?php echo $ticketInformation[0]['contractor_description'] ?></p>
					</div>
				</div>
				<div class="panel panel-primary">
					<h1>Completion Date</h1>
					<p>Completion Date: <?php echo $ticketInformation[0]['complete_date'] ?></p>
				</div>
				<div class="panel panel-info">
					<h1>Your Notes</h1>
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#editLandlordDescription">EDIT</button>
					<p><?php echo $ticketInformation[0]['landlord_description'] ?></p>
				</div>
			<?php } ?>
			<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">Resolve</button>
		</div>
	</div>
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<div class="col-md-12">
						<div class="col-md-6">
							<h4 class="modal-title">Resolve Ticket</h4>
							<?php
							$ticketInformation[0]['created_date'] = explode(" ", $ticketInformation[0]['created_date'])[0];
							$ticketInformation[0]['created_date'] = date('d.m.Y', strtotime($ticketInformation[0]['created_date']));
							?>
							<h6>Data Received: <?php echo $ticketInformation[0]['created_date'] ?></h6>
							<p><span style="color: blue">Property: </span><?php echo $ticketInformation[1]['address'] ?></p>
						</div>
						<div class="col-md-6">
							<h4 style="font-family: gabriola">Tenants: </h4>
						</div>
					</div>
				</div>
				<div class="modal-body">
					<form method="post" action="">
						<div class="col-md-6">
							<input type="date" name="dateOfCompletion">
						</div>
						<div class="col-md-6">
							<select>
								<option selected="" disabled="">Completed by</option>
							</select>
						</div>
						<div class="col-md-12">
							<textarea name="landlord_description"></textarea>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div id="sendToContractor" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="col-md-12">
						<div class="col-md-12 col-md-offset-4">
							<h4 class="modal-title">Assign to Contractor</h4>
						</div>
						<div class="col-md-12">
							<div class="col-md-6">
								<?php
								$ticketInformation[0]['created_date'] = explode(" ", $ticketInformation[0]['created_date'])[0];
								$ticketInformation[0]['created_date'] = date('d.m.Y', strtotime($ticketInformation[0]['created_date']));
								?>
								<h6>Data Received: <?php echo $ticketInformation[0]['created_date'] ?></h6>
								<p><span style="color: blue">Property: </span><?php echo $ticketInformation[1]['address'] ?></p>
							</div>
							<div class="col-md-6">
								<h4 style="font-family: gabriola">Tenants: </h4>
							</div>
						</div>
					</div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="post" action="<?php echo base_url() . 'landlord/send_to_team_member' ?>">
						<select id="selectTeamMember" name="member">
							<option selected="" disabled="">Select team member</option>
							<?php foreach ($landlordTeam as $member): ?>
								<option value="<?php echo $member['user_id'] . '.' . $ticketInformation[0]['ticket_id'] ?>"><?php echo $member['first_name'] . ' ' . $member['last_name'] ?></option>
							<?php endforeach ?>
						</select>
						<input class="btn btn-danger" type="submit" value="SEND TICKET" disabled="">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div id="editLandlordDescription" class="modal fade" role="dialog">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<div class="col-md-12 col-md-offset-4">
						<h1>Edit Description</h1>
					</div>
					<button type="button" class="close" data-dismiss="modal">&times;</button>
				</div>
				<div class="modal-body">
					<form method="post" action="<?php echo base_url() . 'landlord/editLandlordDescription' ?>">
						<textarea name="newDesc" placeholder="<?php echo $ticketInformation[0]['landlord_description'] ?>"></textarea>
						<input type="submit" value="UPDATE DESCRIPTION">
						<input type="hidden" name="ticketID" value="<?php echo $ticketInformation[0]['ticket_id'] ?>">
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	$(document).ready(function(){
		$('#selectTeamMember').on('change', function(){
			$(this).siblings('input').removeAttr('disabled');
		})
	})
</script>
</html>