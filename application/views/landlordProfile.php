<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '1'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Landlord Profile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<button class="btn btn-danger"><a style="text-decoration: none" href="<?php echo base_url() . 'user/logOut' ?>">Log Out</a></button>
	<button style="background: orange;border: 0"><a style="text-decoration: none" href="<?php echo base_url() . 'landlord/team' ?>">Team</a></button>
	<button style="background: green;border: 0"><a style="text-decoration: none" href="<?php echo base_url() . 'landlord/add_team_member' ?>">Add Team Member</a></button>
	<h1>Landloard Dashboard</h1><br>
	<div class="container">
		<input id="searchUnallocatedTickets" type="text" placeholder="Search">
		<select id="sortUnallocatedTickets">
			<option selected="" disabled="">Sort</option>
			<option value="NF">Newest First</option>
			<option value="OF">Oldest First</option>
			<option value="HF">Highest Priority First</option>
			<option value="LF">Lowest Priority First</option>
		</select>
		<select class="selectpicker" id="filterUnallocatedTickets">
			<option selected="" disabled="">Filter</option>
  			<optgroup label="ByCategory" data-max-options="2">
    			<option value="1C">Plumbing</option>
    			<option value="2C">Electrician</option>
    			<option value="3C">Maintenance</option>
    			<option value="4C">Carpentry</option>
    			<option value="5C">Gardening</option>
  			</optgroup>
  			<optgroup label="By Priority" data-max-options="2">
    			<option value="3P">High</option>
    			<option value="2P">Medium</option>
    			<option value="1P">Low</option>
  			</optgroup>
		</select>
		<div id="unallocatedTickets" class="panel panel-default">
			<div class="panel-heading" style="font-size: 20px">Unallocated Tickets</div>
			<?php if ($data['unallocatedTickets']) { ?>
				<?php foreach ($data['unallocatedTickets'] as $ticket) { ?>
					<?php
						$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
						$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
					?>
					<div class="col-md-12 panel panel-primary">
						<div class="col-md-6">
							<label>Tenant Name: </label>
							<p><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></p>
							<label>Photo: </label>
							<?php if(isset($ticket['photo'])){ ?>
								<img class="img-circle" src="<?php echo $ticket['photo'] ?>">
							<?php }else{ ?>
								<p>No Photo</p>
							<?php } ?>
							<label>Address: </label>
							<p><?php echo $ticket['address'] ?></p>
						</div>
						<div class="col-md-6" style="text-align: right">
							<label>Priority: </label>
							<p><?php echo $ticket['priority_name'] ?></p>
							<label>Issue: </label>
							<p><?php echo $ticket['problem_name'] ?> Issue</p>
							<label>Received on: </label>
							<p><?php echo $ticket['created_date'] ?></p>
						</div>
						<button class="viewUnallocatedTicket btn btn-success"><a href="<?php echo base_url() . 'landlord/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>
					</div>
				<?php } ?>
				<button id="seeAllTickets" style="background: orange;color: black;border: 0;cursor: pointer;">See All</button>
				<?php if($data['pageCount']){ ?>
					<ul class="pagination">
						<?php for($i=1;$i<=$data['pageCount'];$i++) { ?>
							<li class="unallocatedTicketsPagination"><a href="javascript: void(0)"><?php echo $i ?></a></li>	
						<?php } ?>
					</ul>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
	<div class="container">
		<input id="searchInProgressTickets" type="text" placeholder="Search">
		<select id="sortInProgressTickets">
			<option selected="" disabled="">Sort</option>
			<option value="NF">Newest First</option>
			<option value="OF">Oldest First</option>
			<option value="HF">Highest Priority First</option>
			<option value="LF">Lowest Priority First</option>
		</select>
		<select class="selectpicker" id="filterInProgressTickets">
			<option selected="" disabled="">Filter</option>
  			<optgroup label="ByCategory" data-max-options="2">
    			<option value="1C">Plumbing</option>
    			<option value="2C">Electrician</option>
    			<option value="3C">Maintenance</option>
    			<option value="4C">Carpentry</option>
    			<option value="5C">Gardening</option>
  			</optgroup>
  			<optgroup label="By Priority" data-max-options="2">
    			<option value="3P">High</option>
    			<option value="2P">Medium</option>
    			<option value="1P">Low</option>
  			</optgroup>
		</select>
		<div id="inProgressTickets" class="panel panel-default">
			<div class="panel-heading" style="font-size: 20px">Tickets In Progress</div>
			<?php if($data['inProgressTickets']){ ?>
				<?php foreach($data['inProgressTickets'] as $ticket){ ?>
					<?php  
						$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
						$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
					?>
					<div class="col-md-12 panel panel-primary">
						<div class="col-md-6">
							<label>Tenant Name: </label>
							<p><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></p>
							<label>Photo: </label>
							<?php if(isset($ticket['photo'])){ ?>
								<img class="img-circle" src="<?php echo $ticket['photo'] ?>">
							<?php }else{ ?>
								<p>No Photo</p>
							<?php } ?>
							<label>Address: </label>
							<p><?php echo $ticket['address'] ?></p>
						</div>
						<div class="col-md-6" style="text-align: right">
							<label>Priority: </label>
							<p><?php echo $ticket['priority_name'] ?></p>
							<label>Issue: </label>
							<p><?php echo $ticket['problem_name'] ?> Issue</p>
							<label>Received on: </label>
							<p><?php echo $ticket['created_date'] ?></p>
						</div>
						<button class="viewUnallocatedTicket btn btn-success"><a href="<?php echo base_url() . 'landlord/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>
					</div>
				<?php } ?>
				<button id="seeAllTickets" style="background: orange;color: black;border: 0;cursor: pointer;">See All</button>
				<?php if($data['pageCount2']){ ?>
					<ul class="pagination">
						<?php for($i=1;$i<=$data['pageCount2'];$i++) { ?>
							<li class="inProgressTicketsPagination"><a href="javascript: void(0)"><?php echo $i ?></a></li>	
						<?php } ?>
					</ul>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			let base_url = "<?php echo base_url() ?>";
			$('#searchUnallocatedTickets').on('input', function(){
				let value = $(this).val().trim();
				let sortValue = $('#sortUnallocatedTickets').val();
				let filterValue = $('#filterUnallocatedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'landlord/search_unallocated_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#unallocatedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Unallocated Tickets</div>");
							header.appendTo('#unallocatedTickets');
							r.data.forEach(function(result){

								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#unallocatedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='unallocatedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#unallocatedTickets');
						}
					}
				})
			})

			$('#searchInProgressTickets').on('input', function(){
				let value = $(this).val().trim();
				let sortValue = $('#sortInProgressTickets').val();
				let filterValue = $('#filterInProgressTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'landlord/search_inprogress_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#inProgressTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Tickets In Progress</div>");
							header.appendTo('#inProgressTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#inProgressTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount2;i++){
								let li = $("<li class='inProgressTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#inProgressTickets');
						}
					}
				})				
			})

			$('#sortUnallocatedTickets').on('change', function(){
				let sortValue = $(this).val();
				let searchValue = $('#searchUnallocatedTickets').val().trim();
				let filterValue = $('#filterUnallocatedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'landlord/sort_unallocated_tickets',
					data: {
						'orderBy': order_by,
						'searchVal': searchValue,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#unallocatedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Unallocated Tickets</div>");
							header.appendTo('#unallocatedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#unallocatedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='unallocatedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#unallocatedTickets');
						}
					}
				})
			})

			$('#sortInProgressTickets').on('change', function(){
				let sortValue = $(this).val();
				let searchValue = $('#searchInProgressTickets').val().trim();
				let filterValue = $('#filterInProgressTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'landlord/sort_inprogress_tickets',
					data: {
						'orderBy': order_by,
						'searchVal': searchValue,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#inProgressTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Tickets In Progress</div>");
							header.appendTo('#inProgressTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#inProgressTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount2;i++){
								let li = $("<li class='inProgressTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#inProgressTickets');
						}
					}
				})
			})

			$('#filterUnallocatedTickets').on('change', function(){
				let filterValue = $(this).val();
				let sortValue = $('#sortUnallocatedTickets').val();
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				let searchValue = $('#searchUnallocatedTickets').val().trim();
				$.ajax({
					type: 'post',
					url: base_url + 'landlord/filter_unallocated_tickets',
					data: {
						'filterValue': filterValue,
						'searchVal': searchValue,
						'orderBy': order_by,
						'desc': desc
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#unallocatedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Unallocated Tickets</div>");
							header.appendTo('#unallocatedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#unallocatedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='unallocatedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#unallocatedTickets');
						}
					}
				})
			})

			$('#filterInProgressTickets').on('change', function(){
				let filterValue = $(this).val();
				let sortValue = $('#sortInProgressTickets').val();
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				let searchValue = $('#searchInProgressTickets').val().trim();
				$.ajax({
					type: 'post',
					url: base_url + 'landlord/filter_inprogress_tickets',
					data: {
						'filterValue': filterValue,
						'searchVal': searchValue,
						'orderBy': order_by,
						'desc': desc
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#inProgressTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Tickets In Progress</div>");
							header.appendTo('#inProgressTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#inProgressTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount2;i++){
								let li = $("<li class='inProgressTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#inProgressTickets');
						}
					}
				})
			})

			$(document).on('click', '.unallocatedTicketsPagination', function(){
				let pageValue = parseInt($(this).find('a').html());
				let value = $('#searchUnallocatedTickets').val().trim();
				let sortValue = $('#sortUnallocatedTickets').val();
				let filterValue = $('#filterUnallocatedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				$.ajax({
					type: 'post',
					url: base_url + 'landlord/pagination_unallocated_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal,
						'page': pageValue
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#unallocatedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Unallocated Tickets</div>");
							header.appendTo('#unallocatedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#unallocatedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='unallocatedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#unallocatedTickets');
						}
					}
				})
			})

			$(document).on('click', '.inProgressTicketsPagination', function(){
				let pageValue = parseInt($(this).find('a').html());
				let value = $('#searchInProgressTickets').val().trim();
				let sortValue = $('#sortInProgressTickets').val();
				let filterValue = $('#filterInProgressTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				$.ajax({
					type: 'post',
					url: base_url + 'landlord/pagination_inprogress_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal,
						'page': pageValue
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#inProgressTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Tickets In Progress</div>");
							header.appendTo('#inProgressTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let label2 = $('<label>Photo: </label>');
								let label21 = $('<label>Address: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let userImg = $('<p>No photo</p>');
								if (result.photo) {
									userImg = $("<img src='"+result.photo+"'>");
								}
								let address = $('<p>' + result.address + '</p>');
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label2.appendTo(div1);
								userImg.appendTo(div1);
								label21.appendTo(div1);
								address.appendTo(div1);
								div1.appendTo(div);
								let label31 = $('<label>Priority: </label>');
								let priorityName = $('<p>'+result.priority_name+'</p>');
								let label32 = $('<label>Issue: </label>');
								let ticketProblem = $("<p>"+result.problem_name+"</p>");
								let label33 = $('<label>Received on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label31.appendTo(div2);
								priorityName.appendTo(div2);
								label32.appendTo(div2);
								ticketProblem.appendTo(div2);
								label33.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a href='"+base_url+"landlord/tickets/"+result.ticket_id+"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#inProgressTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount2;i++){
								let li = $("<li class='inProgressTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#inProgressTickets');
						}
					}
				})
			})

			// $('#searchLandlordRecentlyRasiedTickets').on('input', function(){
			// 	let value = $(this).val().trim();
			// 	$.ajax({
			// 		type: 'post',
			// 		url: base_url + 'landlord/search_recently_rasied_tickets',
			// 		data: {
			// 			inputval: value
			// 		},
			// 		success: function(r){
			// 			if(r){
			// 				r = JSON.parse(r);
			// 				$('#recentlyRasiedTickets').empty();
			// 				r.forEach(function(result){
			// 					result.created_date = result.created_date.split(" ")[0].split("-");
			// 					result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 					let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 					let div1 = $('<div></div>');
			// 					let div2 = $('<div></div>');
			// 					let label1 = $('<label>Tenant Name: </label>');
			// 					let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 					let userImg = $("<img src='"+result.photo+"'>");
			// 					label1.appendTo(div1);
			// 					tenantName.appendTo(div1);
			// 					userImg.appendTo(div1);
			// 					div1.appendTo(div);
			// 					let priorityName = $('<p>'+result.priority_name+'</p>');
			// 					let address = $('<label>Address: '+result.address+'</label>');
			// 					let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 					let label3 = $('<label>Received on: </label>');
			// 					let date = $("<p>"+result.created_date+"</p>");
			// 					priorityName.appendTo(div2);
			// 					address.appendTo(div2);
			// 					ticketProblem.appendTo(div2);
			// 					label3.appendTo(div2);
			// 					date.appendTo(div2);
			// 					div2.appendTo(div);
			// 					div.appendTo('#recentlyRasiedTickets');
			// 				})
			// 			}
			// 		}
			// 	})
			// })

			// $('#searchLandlordOldestUnresolvedTickets').on('input', function(){
			// 	let value = $(this).val().trim();
			// 	$.ajax({
			// 		type: 'post',
			// 		url: base_url + 'landlord/search_oldest_unresolved_tickets',
			// 		data: {
			// 			inputval: value
			// 		},
			// 		success: function(r){
			// 			if(r){
			// 				r = JSON.parse(r);
			// 				$('#oldestUnresolvedTickets').empty();
			// 				r.forEach(function(result){
			// 					result.created_date = result.created_date.split(" ")[0].split("-");
			// 					result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 					let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 					let div1 = $('<div></div>');
			// 					let div2 = $('<div></div>');
			// 					let label1 = $('<label>Tenant Name: </label>');
			// 					let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 					let userImg = $("<img src='"+result.photo+"'>");
			// 					label1.appendTo(div1);
			// 					tenantName.appendTo(div1);
			// 					userImg.appendTo(div1);
			// 					div1.appendTo(div);
			// 					let priorityName = $('<p>'+result.priority_name+'</p>');
			// 					let address = $('<label>Address: '+result.address+'</label>');
			// 					let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 					let label3 = $('<label>Received on: </label>');
			// 					let date = $("<p>"+result.created_date+"</p>");
			// 					priorityName.appendTo(div2);
			// 					address.appendTo(div2);
			// 					ticketProblem.appendTo(div2);
			// 					label3.appendTo(div2);
			// 					date.appendTo(div2);
			// 					div2.appendTo(div);
			// 					div.appendTo('#oldestUnresolvedTickets');
			// 				})
			// 			}
			// 		}
			// 	})
			// })

			// $('#sortRecentlyRasiedTickets').on('change', function(){
			// 	let sortValue = $(this).val();
			// 	let searchValue = $('#searchLandlordRecentlyRasiedTickets').val().trim();
			// 	let order_by;
			// 	let desc = '';
			// 	if(sortValue == 'NF'){
			// 		order_by = 'created_date';
			// 		desc = 'desc';
			// 	}else if(sortValue == 'OF'){
			// 		order_by = 'created_date';
			// 	}else if(sortValue == 'HF'){
			// 		order_by = 'priority';
			// 		desc = 'desc';
			// 	}else if(sortValue == 'LF'){
			// 		order_by = 'priority';
			// 	}
			// 	if(searchValue){
			// 		if(desc == ''){
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_recently_rasied_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'searchVal': searchValue
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#recentlyRasiedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#recentlyRasiedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}else{
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_recently_rasied_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'searchVal': searchValue,
			// 					'desc': desc
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#recentlyRasiedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#recentlyRasiedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}
			// 	}else{
			// 		if(desc == ''){
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_recently_rasied_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#recentlyRasiedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#recentlyRasiedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}else{
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_recently_rasied_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'desc': desc
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#recentlyRasiedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#recentlyRasiedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}
			// 	}
			// })

			// $('#sortOldestUnresolvedTickets').on('change', function(){
			// 	let sortValue = $(this).val();
			// 	let searchValue = $('#searchLandlordOldestUnresolvedTickets').val().trim();
			// 	let order_by;
			// 	let desc = '';
			// 	if(sortValue == 'NF'){
			// 		order_by = 'created_date';
			// 		desc = 'desc';
			// 	}else if(sortValue == 'OF'){
			// 		order_by = 'created_date';
			// 	}else if(sortValue == 'HF'){
			// 		order_by = 'priority';
			// 		desc = 'desc';
			// 	}else if(sortValue == 'LF'){
			// 		order_by = 'priority';
			// 	}
			// 	if(searchValue){
			// 		if(desc == ''){
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_oldest_unresolved_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'searchVal': searchValue
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#oldestUnresolvedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#oldestUnresolvedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}else{
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_oldest_unresolved_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'searchVal': searchValue,
			// 					'desc': desc
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#oldestUnresolvedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#oldestUnresolvedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}
			// 	}else{
			// 		if(desc == ''){
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_oldest_unresolved_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#oldestUnresolvedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#oldestUnresolvedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}else{
			// 			$.ajax({
			// 				type: 'post',
			// 				url: base_url + 'landlord/sort_oldest_unresolved_tickets',
			// 				data: {
			// 					'orderBy': order_by,
			// 					'desc': desc
			// 				},
			// 				success: function(r){
			// 					if(r){
			// 						r = JSON.parse(r);
			// 						$('#oldestUnresolvedTickets').empty();
			// 						r.forEach(function(result){
			// 							result.created_date = result.created_date.split(" ")[0].split("-");
			// 							result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
			// 							let div = $("<div style='border: 1px solid red;float: left;margin-left: 20px'></div>");
			// 							let div1 = $('<div></div>');
			// 							let div2 = $('<div></div>');
			// 							let label1 = $('<label>Tenant Name: </label>');
			// 							let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
			// 							let userImg = $("<img src='"+result.photo+"'>");
			// 							label1.appendTo(div1);
			// 							tenantName.appendTo(div1);
			// 							userImg.appendTo(div1);
			// 							div1.appendTo(div);
			// 							let priorityName = $('<p>'+result.priority_name+'</p>');
			// 							let address = $('<label>Address: '+result.address+'</label>');
			// 							let ticketProblem = $("<p>"+result.problem_name+"</p>");
			// 							let label3 = $('<label>Received on: </label>');
			// 							let date = $("<p>"+result.created_date+"</p>");
			// 							priorityName.appendTo(div2);
			// 							address.appendTo(div2);
			// 							ticketProblem.appendTo(div2);
			// 							label3.appendTo(div2);
			// 							date.appendTo(div2);
			// 							div2.appendTo(div);
			// 							div.appendTo('#oldestUnresolvedTickets');
			// 						})
			// 					}
			// 				}
			// 			})
			// 		}
			// 	}
			// })
		})
	</script>
</html>