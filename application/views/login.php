<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		#error{
			background: red;
			color: black;
			text-align: center;
		}
		#success {
			background: lightgreen;
			color: black;
			text-align: center;
		}
		body{
			background: lightyellow;
		}
	</style>
</head>
<body>
	<div class="col-md-5 col-md-offset-3">
		<?php if($this->session->flashdata('error')) { ?>
			<div id="error" class="col-md-12">
				<p><?php echo $this->session->flashdata('error') ?></p>
			</div>
		<?php } ?>
		<?php if($this->session->flashdata('success_email_message_send')) { ?>
			<div id="success" class="col-md-12">
				<p><?php echo $this->session->flashdata('success_email_message_send') ?></p>
			</div>
		<?php } ?>
		<button class="btn btn-default"><a style="text-decoration: none" href="<?php echo base_url() . 'signup' ?>">Sign Up</a></button>
		<form method="post" action="<?php echo base_url() . 'login/signin' ?>">
			<div class="form-group">
				<label for="email">Email address:</label>
				<input type="text" name="email" class="form-control" id="email">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label>
				<input type="password" name="pass" class="form-control" id="pwd">
			</div>
			<input type="submit" class="btn btn-success">
		</form>
	</div>
</body>
</html>