<!DOCTYPE html>
<html>
<head>
	<title>Maintenance Registration</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		#error{
			background: lightyellow;
			color: red;
		}
		body{
			background: lightyellow;
		}
	</style>
</head>
<body>
	<?php if($this->session->flashdata('error')) { ?>
		<div id="error">
			<p><?php echo $this->session->flashdata('error') ?></p>
		</div>
	<?php } ?>
	<div class="col-md-5 col-md-offset-3">
		<h1>Registration Form</h1>
		<form method="post" action="<?php echo base_url() . 'signup/reg' ?>">
			<div class="form-group">
				<label for="usertype">I’m registering as a</label>
				<select name="regUserType" id="usertype" class="selectpicker">
					<option value="1">Landlord</option>
					<option value="2">Tenant</option>
				</select>
			</div>
			<div class="form-group">
				<label for="first">First Name:</label>
				<input type="text" name="first_name" placeholder="First Name" id="first" class="form-control">
				<label for="last">Last Name:</label>
				<input type="text" name="last_name" placeholder="Last Name" id="last" class="form-control">
			</div>
			<div class="form-group">
				<label for="email">Email:</label>
				<input type="email" name="email" placeholder="Email" id="email" class="form-control"><br>
			</div>
			<div class="form-group">
				<label for="address">Address:</label>
				<input type="text" name="address" placeholder="Address" id="address" class="form-control">
			</div>
			<div class="form-group">
				<label for="pwd">Password:</label>
				<input type="password" name="pass" placeholder="Password" id="pwd" class="form-control"><br>
			</div>
			<div class="form-group">
				<label for="pwdconf">Password Confirm:</label>
				<input type="password" name="pass_confirm" placeholder="Confirm Password" id="pwdconf" class="form-control">
			</div>
			<div class="form-group">
				<label for="phone">Phone Number:</label>
				<input type="text" name="phone_number" placeholder="Phone Number" class="form-control" id="phone">
			</div>
			<input type="submit" class="btn btn-success form-control">
		</form>
	</div>
</body>
</html>