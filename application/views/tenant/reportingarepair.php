<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '2'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Reporting a Repair</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<style>
		#problem > div {
			cursor: pointer;
		}
	</style>
</head>
<body>
	<h1>Reporting a Repair</h1>
	<form method="post" action="<?php echo base_url() . 'tenant/add_ticket' ?>" enctype="multipart/form-data">
		<div id="problem" style="border: 1px solid blue">
			<h1>What is the problem</h1>
			<h6>Select from the options below so we can help narrow down the problem</h6>
			<div class="problem_name" data-id="1">
				<h4>Lightbulb</h4>
			</div>
			<div class="problem_name" data-id="2">
				<h4>Internet</h4>
			</div>
			<div class="problem_name" data-id="3">
				<h4>Bathroom</h4>
			</div>
			<div class="problem_name" data-id="4">
				<h4>Heating</h4>
			</div>
			<div class="problem_name" data-id="5">
				<h4>Pests/Vermin</h4>
			</div>
			<div class="problem_name" data-id="6">
				<h4>Water Leak</h4>
			</div>
			<div class="problem_name" data-id="7">
				<h4>Boiler</h4>
			</div>
			<div class="problem_name" data-id="8">
				<h4>Smell Gas</h4>
			</div>
			<div class="problem_name" data-id="9">
				<h4>Fire</h4>
			</div>
			<div class="problem_name" data-id="10">
				<h4>Other</h4>
			</div>
		</div>
		<div id="emergency" style="display: none;border: 1px solid blue">
			<h1>Is this an emergency?</h1>
			<h3>If this problem is an emergency you must call your landlord below.</h3>
			<h4 style="color: orange">If it is life-threatening call 999 immediately.</h4>
			<div style="background: lightgreen;border: 1px solid orange">
				<img src="<?php echo $data['landlord']['photo'] ?>">
				<p>Landlord Name: <?php echo $data['landlord']['first_name'] . ' ' . $data['landlord']['last_name'] ?></p>
				<p>Phone: <?php echo $data['landlord']['phone'] ?></p>
				<p>Email: <?php echo $data['landlord']['email'] ?></p>
			</div>
			<button type="button" id="notEmergency" style="background: orange;color: black;width: 320px;height: 25px;">CLICK HERE IF THIS IS NOT AN EMERGENCY</button>
		</div>
		<div id="problemImages" style="display: none;border: 1px solid blue">
			<h1 style="color: lightblue">Let`s take a look at the problem</h1>
			<h1 style="font-family: cursive;">Please add some images below to clarify what the problem is.</h1>
			<div>
				<p>Image1</p>
				<input type="file" name="photo1">
			</div>
			<div>
				<p>Image2</p>
				<input type="file" name="photo2">
			</div>
			<div>
				<p>Image3</p>
				<input type="file" name="photo3">
			</div>
			<div>
				<p>Image4</p>
				<input type="file" name="photo4">
			</div>
			<div>
				<p>Image5</p>
				<input type="file" name="photo5">
			</div>
			<button type="button" id="imgUploadsConfirm">Next</button>
		</div>
		<div id="problemDescription" style="display: none;border: 1px solid blue">
			<h1>Tell us a bit more about the problem</h1>
			<h2>Give as much detail as you can about the problem so that we can get the right person to you.</h2>
			<div class="form-group">
				<textarea class="form-control" name="briefDescription" placeholder="Short Description"></textarea>
			</div>
			<div class="form-group">
				<textarea class="form-control" name="problemDescription" placeholder="Start typing here..."></textarea>
			</div>
			<button type="button" id="aboutProblem">Continue</button>
		</div>
		<div id="emailConfirm" style="display: none;border: 1px solid blue">
			<h1>Please confirm your details</h1>
			<p>Is this your current email address?</p>
			<div>
				<input id="yourEmail" type="text" name="emailConfirm" placeholder="<?php echo $data['user']['email'] ?>" disabled="">
				<button type="button" id="editEmail">Edit</button>
			</div>
			<button type="button" id="confirmEmailAddress">Confirm</button>
		</div>
		<div id="phoneNumber" style="display: none;border: 1px solid blue">
			<h1>Please confirm your details</h1>
			<p>Is this your current phone number?</p>
			<div>
				<input id="yourPhone" type="text" name="phoneConfirm" placeholder="<?php echo $data['user']['phone'] ?>" disabled="">
				<button type="button" id="phoneEdit">Edit</button>
			</div>
			<button type="button" id="confirmPhoneNumber">Confirm</button>
		</div>
		<div id="addressConfirm" style="display: none;border: 1px solid blue">
			<h1>Is this the correct property address for this reapir?</h1>
			<div>
				<input id="yourAddress" type="text" name="addressConfirm" placeholder="<?php echo $data['user']['address'] ?>" disabled="">
				<button type="button" id="addressEdit">Edit</button>
			</div>
			<button type="button" id="confirmAddress">Confirm</button>
		</div>
		<div id="submitTheProblem" style="display: none;border: 1px solid blue">
			<h1 style="color: lightblue">Time to submit the problem</h1>
			<h3>The last step is to to let us know how you'd like contractors to proceed with repairing the problem.</h3>
			<input type="checkbox" name="mark" value="1" checked="">Please give a minimum of 24 hours notice to me and any other tenants.<br>
			<input type="checkbox" name="mark" value="2">I give consent for the contractor to enter the property even if I am not present, please attend to the issue urgently.<br><br><br>
			<input type="checkbox" name="iAgree" id="iAgree"> I agree to the terms & conditions
			<input type="submit" disabled="" class="btn btn-primary" value="Add Ticket">
		</div>
	</form>
</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			$('.problem_name').click(function(){
				let value = $(this).attr('data-id');
				let input = $("<input type='hidden' name='problemCategory' value='"+value+"'>");
				input.appendTo($(this));
				if(value == '5' || value == '10' || value == '3' || value == '2'){
					let inp = $("<input type='hidden' name='ticketCategory' value='3'>");
					inp.appendTo($(this));
				}else if(value == '6' || value == '7' || value == '4'){
					let inp = $("<input type='hidden' name='ticketCategory' value='1'>");
					inp.appendTo($(this));
				}
				if(value == '8' || value == '9'){
					$('#problem').css('display', 'none');
					$('#emergency').css('display', 'block');
				}else{
					$('#problem').css('display', 'none');
					$('#problemImages').css('display', 'block');
				}
			})
			$('#notEmergency').click(function(){
				$('#emergency').css('display', 'none');
				$('#problemImages').css('display', 'block');
			})
			$('#imgUploadsConfirm').click(function(){
				$('#problemImages').css('display', 'none');
				$('#problemDescription').css('display', 'block');
			})
			$('#aboutProblem').click(function(){
				$('#problemDescription').css('display', 'none');
				$('#emailConfirm').css('display', 'block');
			})
			$('#confirmEmailAddress').click(function(){
				$('#emailConfirm').css('display', 'none');
				$('#phoneNumber').css('display', 'block');
			})
			$('#confirmPhoneNumber').click(function(){
				$('#phoneNumber').css('display', 'none');
				$('#addressConfirm').css('display', 'block');
			})
			$('#confirmAddress').click(function(){
				$('#addressConfirm').css('display', 'none');
				$('#submitTheProblem').css('display', 'block');
			})
			$('#editEmail').click(function(){
				$('#yourEmail').removeAttr('disabled');
			})
			$('#phoneEdit').click(function(){
				$('#yourPhone').removeAttr('disabled');
			})
			$('#addressEdit').click(function(){
				$('#yourAddress').removeAttr('disabled');
			})
			$('#iAgree').change(function(event){
				let checkbox = event.target;
				if(checkbox.checked){
					$(this).siblings("input[type = 'submit']").removeAttr('disabled');
				}else{
					$(this).siblings("input[type = 'submit']").attr('disabled', "");
				}
			})
		})
	</script>
</html>