<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '2'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tickets</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<div class="container">
		<div class="col-md-6 col-md-offset-3">
			<?php if($tickets['ticketInformation']){ ?>
				<div class="col-md-12 col-md-offset-3">
					<h1 style="font-family: gabriola">Repair Ticket (#<?php echo $tickets['ticketInformation'][0]['ticket_id'] ?>)</h1><br>
				</div>
				<?php 
					$tickets['ticketInformation'][1]['created_date'] = explode(" ", $tickets['ticketInformation'][1]['created_date'])[0];
					$tickets['ticketInformation'][1]['created_date'] = date('d.m.Y', strtotime($tickets['ticketInformation'][1]['created_date']));
				?>
				<h5 class="col-md-12 col-md-offset-3" style="font-family: cursive;">Date received: <?php echo $tickets['ticketInformation'][1]['created_date'] ?></h5>
				<h5 class="col-md-12 col-md-offset-3" style="font-family: cursive;">Property: <?php echo $tickets['ticketInformation'][1]['address'] ?></h5>
				<h5 class="col-md-12 col-md-offset-3" style="font-family: cursive;">Category: <?php echo $tickets['ticketInformation'][0]['category'] ?></h5>
				<h1 style="font-family: gabriola">Reported By: </h1>
				<div class="col-md-12">
					<?php foreach ($tickets['ticketInformation'] as $tenant) { ?>
						<?php if($tenant['user_type'] == '2'){ ?>						
							<div class="col-md-6">
								<img src="<?php echo $tenant['photo'] ?>" class="img-circle">
								<h3><?php echo $tenant['first_name'] . ' ' . $tenant['last_name'] ?></h3>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
				<div class="col-md-12 col-md-offset-3">
					<h1 style="font-family: gabriola">Problem Reported</h1>
					<h6><i><?php echo $tickets['ticketInformation'][0]['tenant_short_description'] ?></i></h6>
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#newDescription">Edit</button>
					<p style="font-family: cursive" class="col-md-12 col-md-offset-1"><?php echo $tickets['ticketInformation'][0]['tenant_description'] ?></p>
				</div>
				<div>
					<h1 style="font-family: gabriola" class="col-md-12 col-md-offset-3">Photographs</h1>
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#newPhotoModal">Add New Photo</button>
					<?php if($tickets['ticketInformation'][0]['photos']){ ?>
						<?php
							$tickets['ticketInformation'][0]['photos'] = json_decode($tickets['ticketInformation'][0]['photos']);
						?>
						<?php foreach ($tickets['ticketInformation'][0]['photos'] as $photo) { ?>
							<img class="img-thumbnail" width="200" height="200" src="<?php echo base_url() . 'assets/uploads/img/' . $photo . '.jpg' ?>">
						<?php } ?>
					<?php } ?>
				</div>
				<div class="panel panel-primary">
					<h1>Contractors</h1>
					<h5>Notice: Marked as  '<?php echo $tickets['ticketInformation'][0]['mark'] ?>'</h5>
					<div class="col-md-12">
						<?php foreach ($tickets['ticketInformation'] as $contractor) { ?>
							<?php if($contractor['user_type'] == '3'){ ?>						
								<div class="col-md-6">
									<img src="<?php echo $contractor['photo'] ?>" class="img-circle">
									<h3><?php echo $contractor['first_name'] . ' ' . $contractor['last_name'] ?></h3>
									<p><?php echo $contractor['name'] ?></p>
									<h3>Phone: <?php echo $contractor['phone'] ?></h3>
									<h3>Email: <?php echo $contractor['email'] ?></h3>
								</div>
							<?php } ?>
						<?php } ?>
					</div>
					<div>
						<h1>Contractor Notes</h1>
						<p><?php echo $tickets['ticketInformation'][0]['contractor_description'] ?></p>
					</div>
				</div>
				<div class="panel panel-primary">
					<h1>Completion Date</h1>
					<p>Completion Date: <?php echo $tickets['ticketInformation'][0]['complete_date'] ?></p>
				</div>
				<div class="panel panel-info">
					<h1>Your Notes</h1>
					<p><?php echo $tickets['ticketInformation'][0]['landlord_description'] ?></p>
				</div>
			<?php } ?>
		</div>
	</div>
	<div class="col-md-3">
		<div class="col-md-12" style="border: 1px solid lightblue">
			<h1>Completion Date Arranged For</h1>
			<?php echo $tickets['ticketInformation'][0]['complete_date'] ?>
			<button type="button" data-toggle="modal" data-target="#confirmWorkDate" class="btn" style="background: #e23400;color: black;font-family: gabriola">CONFIRM</button>
		</div>
	</div>
	<div id="newDescription" class="modal fade" role="dialog">
  		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<div class="col-md-12 col-md-offset-4">
					<h4 class="modal-title">Edit your description</h4>
		      	</div>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<form method="post" action="<?php echo base_url() . 'tenant/editTenantDescription' ?>">
		      		<div class="form-group">
		      			<textarea class="form-control" name="newDesc" placeholder="<?php echo $tickets['ticketInformation'][0]['tenant_description'] ?>"></textarea>
		      		</div>
		      		<input type="hidden" name="ticketID" value="<?php echo $tickets['ticketInformation'][1]['ticket_id'] ?>">
		      		<input class="btn btn-primary" type="submit" value="Edit">
		      	</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
  		</div>
	</div>
	<div id="newPhotoModal" class="modal fade" role="dialog">
  		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<div class="col-md-12 col-md-offset-4">
					<h4 class="modal-title">Add New Photo</h4>
		      	</div>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<form method="post" action="<?php echo base_url() . 'tenant/addNewProblemPhoto' ?>" enctype="multipart/form-data">
		      		<input type="file" name="newPhoto">
		      		<input type="hidden" name="ticketID" value="<?php echo $tickets['ticketInformation'][1]['ticket_id'] ?>">
		      		<input class="btn btn-primary" type="submit" value="SAVE">
		      	</form>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
  		</div>
	</div>
	<div id="confirmWorkDate" class="modal fade" role="dialog">
  		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<div class="col-md-12 col-md-offset-4">
					<h4 class="modal-title" style="font-family: cursive;">Confirm Work Date</h4>
		      	</div>
				<p>If you have any objections to the date of the contractor is coming, please rearrange it below.	</p>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<h5>Arranged for <?php echo $tickets['ticketInformation'][0]['complete_date'] ?></h5>
		      	<form method="post" action="<?php echo base_url() . 'tenant/addNewProblemPhoto' ?>" enctype="multipart/form-data">
		      		<input type="submit" value="CONFIRM" class="btn" style="border-radius: 15%;background: #e23400;color: black;font-family: cursive;">
		      	</form>
		      	<button type="button" class="close btn btn-primary" data-toggle="modal" data-target="#rearrangeWorkDate" data-dismiss="modal">REARRANGE</button>
		      </div>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
  		</div>
	</div>
	<div id="rearrangeWorkDate" class="modal fade" role="dialog">
  		<div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		      	<div class="col-md-12 col-md-offset-4">
					<h4 class="modal-title" style="font-family: cursive;">Rearrange Work Date</h4>
		      	</div>
				<p>Please select a time and date when you know you or your housemates will be in.</p>
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		      </div>
		      <div class="modal-body">
		      	<p><span style="font-family: cursive;">Currently Arranged for</span> <?php echo $tickets['ticketInformation'][0]['complete_date'] ?></p>
		      	<form>
		      		<h3 style="font-family: gabriola">Select date and time</h3>
		      		<select name="changeDateAndTime">
		      			<option selected="" disabled=""><?php echo $tickets['ticketInformation'][0]['complete_date'] ?></option>
		      		</select>
		      		<textarea name="description" placeholder="Message"></textarea>
		      	</form>
		      <div class="modal-footer">
		        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		      </div>
		    </div>
  		</div>
	</div>
</body>
</html>