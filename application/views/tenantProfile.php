<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php
	if(!$this->session->userdata('user') || $this->session->userdata('user')['user_type'] != '2'){
		redirect('login');
	}	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Tenant Profile</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>
	<button class="btn btn-danger"><a style="font-size: 18px" href="<?php echo base_url() . 'user/logOut' ?>" style="text-decoration: none;color: black">Log Out</a></button>
	<button class="btn btn-success"><a style="font-size: 18px" href="<?php echo base_url() . 'tenant/reportingarepair' ?>" style="text-decoration: none;color: black">Add Ticket</a></button>
	<h1>Tenant Dashboard</h1>
	<div>
		<input id="searchTenantCurrentTickets" type="text" placeholder="Search" class="form-control">
		<select id="sortCurrentTickets">
			<option selected="" disabled="">Sort</option>
			<option value="NF">Newest First</option>
			<option value="OF">Oldest First</option>
			<option value="HF">Highest Priority First</option>
			<option value="LF">Lowest Priority First</option>
		</select>
		<select class="selectpicker" id="filterCurrentTickets">
			<option selected="" disabled="">Filter</option>
  			<optgroup label="ByCategory" data-max-options="2">
    			<option value="1C">Plumbing</option>
    			<option value="2C">Electrician</option>
    			<option value="3C">Maintenance</option>
    			<option value="4C">Carpentry</option>
    			<option value="5C">Gardening</option>
  			</optgroup>
  			<optgroup label="By Priority" data-max-options="2">
    			<option value="3P">High</option>
    			<option value="2P">Medium</option>
    			<option value="1P">Low</option>
  			</optgroup>
		</select>
		<div id="currentTickets" class="panel panel-default">
			<div class="panel-heading" style="font-size: 20px">Current Tickets</div>
			<?php if ($data['current_tickets']) { ?>
				<?php foreach ($data['current_tickets'] as $ticket) { ?>
					<?php
						$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
						$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
					?>
					<div class="col-md-12 panel panel-primary">
						<div class="col-md-6">
							<label>Tenant Name: </label>
							<p><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></p>
							<label>Issue: </label>
							<h5><?php echo $ticket['category'] ?> Issue</h5>
							<label>Photo: </label>
							<?php if(isset($ticket['photo'])){ ?>
								<img class="img-circle" src="<?php echo $ticket['photo'] ?>">
							<?php }else{ ?>
								<p>No Photo</p>
							<?php } ?>
						</div>
						<div class="col-md-6" style="text-align: right">
							<label>Status: </label>
							<p><?php echo $ticket['ticket_status'] ?></p>
							<label>Sent on: </label>
							<p><?php echo $ticket['created_date'] ?></p>
						</div>
						<button class="viewUnallocatedTicket btn btn-success"><a style="text-decoration: none;" href="<?php echo base_url() . 'tenant/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>
					</div>
				<?php } ?>
				<?php if($data['current_tickets_pages_count']){ ?>
					<ul class="pagination">
						<?php for($i=1;$i<=$data['current_tickets_pages_count'];$i++) { ?>
							<li class="currentTicketsPagination"><a href="javascript: void(0)"><?php echo $i ?></a></li>	
						<?php } ?>
					</ul>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
	<div>
		<input id="searchTenantResolvedTickets" type="text" placeholder="Search" class="form-control">
		<select id="sortResolvedTickets">
			<option selected="" disabled="">Sort</option>
			<option value="NF">Newest First</option>
			<option value="OF">Oldest First</option>
			<option value="HF">Highest Priority First</option>
			<option value="LF">Lowest Priority First</option>
		</select>
		<select class="selectpicker" id="filterResolvedTickets">
			<option selected="" disabled="">Filter</option>
  			<optgroup label="ByCategory" data-max-options="2">
    			<option value="1C">Plumbing</option>
    			<option value="2C">Electrician</option>
    			<option value="3C">Maintenance</option>
    			<option value="4C">Carpentry</option>
    			<option value="5C">Gardening</option>
  			</optgroup>
  			<optgroup label="By Priority" data-max-options="2">
    			<option value="3P">High</option>
    			<option value="2P">Medium</option>
    			<option value="1P">Low</option>
  			</optgroup>
		</select>
		<div id="resolvedTickets" class="panel panel-default">
			<div class="panel-heading" style="font-size: 20px">Resolved Tickets</div>
			<?php if ($data['resolved_tickets']) { ?>
				<?php foreach ($data['resolved_tickets'] as $ticket) { ?>
					<?php
						$ticket['created_date'] = explode(" ", $ticket['created_date'])[0];
						$ticket['created_date'] = date('d.m.Y', strtotime($ticket['created_date']));
					?>
					<div class="col-md-12 panel panel-info">
						<div class="col-md-6">
							<label>Tenant Name: </label>
							<p><?php echo $ticket['first_name'] . ' ' . $ticket['last_name'] ?></p>
							<label>Issue: </label>
							<h5><?php echo $ticket['category'] ?> Issue</h5>
							<label>Photo: </label>
							<?php if(isset($ticket['photo'])){ ?>
								<img class="img-circle" src="<?php echo $ticket['photo'] ?>">
							<?php }else{ ?>
								<p>No Photo</p>
							<?php } ?>
						</div>
						<div class="col-md-6" style="text-align: right">
							<label>Status: </label>
							<p><?php echo $ticket['ticket_status'] ?></p>
							<label>Sent on: </label>
							<p><?php echo $ticket['created_date'] ?></p>
						</div>
						<button class="viewUnallocatedTicket btn btn-success"><a style="text-decoration: none;" href="<?php echo base_url() . 'tenant/tickets/' . $ticket['ticket_id'] ?>">View Ticket</a></button>
					</div>
				<?php } ?>
				<?php if($data['resolved_tickets_pages_count']){ ?>
					<ul class="pagination">
						<?php for($i=1;$i<=$data['resolved_tickets_pages_count'];$i++) { ?>
							<li class="resolvedTicketsPagination"><a href="javascript: void(0)"><?php echo $i ?></a></li>	
						<?php } ?>
					</ul>
				<?php } ?>
			<?php } ?>
		</div>
	</div>
</body>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script>
		$(document).ready(function(){
			let base_url = "<?php echo base_url() ?>";
			$('#searchTenantCurrentTickets').on('input', function(){
				let value = $(this).val().trim();
				let sortValue = $('#sortCurrentTickets').val();
				let filterValue = $('#filterCurrentTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/search_current_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#currentTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Current Tickets</div>");
							header.appendTo('#currentTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#currentTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='currentTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#currentTickets');
						}
					}
				})
			})

			$('#searchTenantResolvedTickets').on('input', function(){
				let value = $(this).val().trim();
				let sortValue = $('#sortResolvedTickets').val();
				let filterValue = $('#filterResolvedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/search_resolved_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#resolvedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Resolved Tickets</div>");
							header.appendTo('#resolvedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#resolvedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='resolvedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#resolvedTickets');
						}
					}
				})
			})

			$('#sortCurrentTickets').on('change', function(){
				let sortValue = $(this).val();
				let searchValue = $('#searchTenantCurrentTickets').val().trim();
				let filterValue = $('#filterCurrentTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/sort_current_tickets',
					data: {
						'orderBy': order_by,
						'searchVal': searchValue,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#currentTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Current Tickets</div>");
							header.appendTo('#currentTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#currentTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='currentTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#currentTickets');
						}
					}
				})
			})

			$('#sortResolvedTickets').on('change', function(){
				let sortValue = $(this).val();
				let searchValue = $('#searchTenantResolvedTickets').val().trim();
				let filterValue = $('#filterResolvedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/sort_resolved_tickets',
					data: {
						'orderBy': order_by,
						'searchVal': searchValue,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#resolvedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Resolved Tickets</div>");
							header.appendTo('#resolvedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#resolvedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='resolvedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#resolvedTickets');
						}
					}
				})
			})

			$('#filterCurrentTickets').on('change', function(){
				let filterValue = $(this).val();
				let sortValue = $('#sortCurrentTickets').val();
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				let searchValue = $('#searchTenantCurrentTickets').val().trim();
				$.ajax({
					type: 'post',
					url: base_url + 'tenant/filterCurrentTickets',
					data: {
						'filterValue': filterValue,
						'searchVal': searchValue,
						'orderBy': order_by,
						'desc': desc
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#currentTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Current Tickets</div>");
							header.appendTo('#currentTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#currentTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='currentTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#currentTickets');
						}
					}
				})
			})

			$('#filterResolvedTickets').on('change', function(){
				let filterValue = $(this).val();
				let sortValue = $('#sortResolvedTickets').val();
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}
				let searchValue = $('#searchTenantResolvedTickets').val().trim();
				$.ajax({
					type: 'post',
					url: base_url + 'tenant/filterResolvedTickets',
					data: {
						'filterValue': filterValue,
						'searchVal': searchValue,
						'orderBy': order_by,
						'desc': desc
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#resolvedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Resolved Tickets</div>");
							header.appendTo('#resolvedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#resolvedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='resolvedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#resolvedTickets');
						}
					}
				})
			})

			$(document).on('click', '.currentTicketsPagination', function(){
				let pageValue = parseInt($(this).find('a').html());
				let value = $('#searchTenantCurrentTickets').val().trim();
				let sortValue = $('#sortCurrentTickets').val();
				let filterValue = $('#filterCurrentTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/pagination_current_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal,
						'page': pageValue
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#currentTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Current Tickets</div>");
							header.appendTo('#currentTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#currentTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='currentTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#currentTickets');
						}
					}
				})
			})

			$(document).on('click', '.resolvedTicketsPagination', function(){
				let pageValue = parseInt($(this).find('a').html());
				let value = $('#searchTenantResolvedTickets').val().trim();
				let sortValue = $('#sortResolvedTickets').val();
				let filterValue = $('#filterResolvedTickets').val();
				let category;
				let filterVal;
				if(filterValue){
					category = filterValue[1];
					filterVal = filterValue[0];
				}
				let order_by;
				let desc = '';
				if(sortValue == 'NF'){
					order_by = 'created_date';
					desc = 'desc';
				}else if(sortValue == 'OF'){
					order_by = 'created_date';
				}else if(sortValue == 'HF'){
					order_by = 'priority';
					desc = 'desc';
				}else if(sortValue == 'LF'){
					order_by = 'priority';
				}

				$.ajax({
					type: 'post',
					url: base_url + 'tenant/pagination_resolved_tickets',
					data: {
						'inputval': value,
						'orderBy': order_by,
						'desc': desc,
						'filterCategory': category,
						'filterValue': filterVal,
						'page': pageValue
					},
					success: function(r){
						if(r){
							r = JSON.parse(r);
							$('#resolvedTickets').empty();
							let header = $("<div class='panel-heading' style='font-size: 20px'>Resolved Tickets</div>");
							header.appendTo('#resolvedTickets');
							r.data.forEach(function(result){
								result.created_date = result.created_date.split(" ")[0].split("-");
								result.created_date = result.created_date[2] + '.' + result.created_date[1] + '.' + result.created_date[0];
								let div = $("<div class='col-md-12 panel panel-primary'></div>");
								let div1 = $("<div class='col-md-6'></div>");
								let div2 = $("<div class='col-md-6' style='text-align: right'></div>");
								let label1 = $('<label>Tenant Name: </label>');
								let tenantName = $("<p>"+result.first_name+' '+result.last_name+"</p>");
								let label11 = $('<label>Issue: </label>');
								let category = $("<h5>"+result.category+" Issue</h5>");
								let label12 = $('<label>Photo: </label>');
								let userImg = $('<p>No Photo</p>');
								if(result.photo){
									userImg = $("<img class='img-circle' src='"+result.photo+"'>");	
								}
								label1.appendTo(div1);
								tenantName.appendTo(div1);
								label11.appendTo(div1);
								category.appendTo(div1);
								label12.appendTo(div1);
								userImg.appendTo(div1);
								div1.appendTo(div);
								let label2 = $('<label>Status: </label>');
								let ticketStatus = $("<p>"+result.ticket_status+"</p>");
								let label3 = $('<label>Sent on: </label>');
								let date = $("<p>"+result.created_date+"</p>");
								label2.appendTo(div2);
								ticketStatus.appendTo(div2);
								label3.appendTo(div2);
								date.appendTo(div2);
								div2.appendTo(div);
								let button = $("<button class='viewUnallocatedTicket btn btn-success'><a style='text-decoration: none;' href='" + base_url + 'tenant/tickets/' + result.ticket_id +"'>View Ticket</a></button>");
								button.appendTo(div);
								div.appendTo('#resolvedTickets');
							})
							let ul = $("<ul class='pagination'></ul>");
							for(let i=1;i<=r.pageCount;i++){
								let li = $("<li class='resolvedTicketsPagination'><a href='javascript: void(0)'>"+i+"</a></li>");
								li.appendTo(ul);
							}
							ul.appendTo('#resolvedTickets');
						}
					}
				})
			})			
		})
	</script>
</html>